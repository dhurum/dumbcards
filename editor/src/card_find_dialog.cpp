/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "card_find_dialog.h"
#include <QCheckBox>
#include <QComboBox>
#include <QLineEdit>
#include <QVBoxLayout>
#include "card.h"
#include "card_tags_selector.h"
#include "qt_helpers.h"

CardFindDialog::CardFindDialog(const std::vector<std::unique_ptr<Card>> &cards,
                               const std::vector<Tag> &tags)
    : cards(cards), tags(tags) {
  makeGui();
  setWindowTitle(tr("Find Cards"));
}

void CardFindDialog::addInputs(QVBoxLayout *inputs_layout) {
  match_all_checkbox = new QCheckBox(tr("Search all fields"));
  inputs_layout->addWidget(match_all_checkbox);

  inputs_layout->addWidget(new QLabel(tr("With tags:")));
  tags_selector = new CardTagsSelector(tags, nullptr);
  inputs_layout->addWidget(tags_selector);

  tags_type = new QComboBox();
  tags_type->addItem(tr("With any of the tags"));
  tags_type->addItem(tr("With all of the tags"));
  inputs_layout->addWidget(tags_type);
}

static bool matchCard(const std::unique_ptr<Card> &card,
                      const QString &match_str, bool match_case,
                      bool full_match, bool match_all,
                      const std::vector<int64_t> &match_tags, bool all_tags) {
  return card->match(match_all, match_str, match_case, full_match, match_tags,
                     all_tags);
}

void CardFindDialog::find(bool forward) {
  matchOne(cards, forward, matchCard,
           (match_all_checkbox->checkState() == Qt::Checked),
           tags_selector->get(), (tags_type->currentIndex() == 1));
}

void CardFindDialog::selectAll() {
  matchAll(cards, matchCard, (match_all_checkbox->checkState() == Qt::Checked),
           tags_selector->get(), (tags_type->currentIndex() == 1));
}
