/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <QCheckBox>
#include <QLineEdit>

template <typename Container_T, typename Callback_T, typename... Arg_Ts>
void FindDialog::matchOne(const Container_T &container, bool forward,
                          Callback_T callback, Arg_Ts &&... callback_params) {
  if (container.empty()) {
    return;
  }

  QString match_str = search_field->text();
  bool match_case = (match_case_checkbox->checkState() == Qt::Checked);
  bool full_match = (full_match_checkbox->checkState() == Qt::Checked);

  for (size_t i = 0; i < container.size(); ++i) {
    if (forward) {
      if (current_id >= static_cast<int>(container.size() - 1)) {
        current_id = 0;
      } else {
        ++current_id;
      }
    } else {
      if ((current_id <= 0)
          || (current_id > static_cast<int>(container.size()))) {
        current_id = container.size() - 1;
      } else {
        --current_id;
      }
    }

    if (callback(container[current_id], match_str, match_case, full_match,
                 std::forward<Arg_Ts>(callback_params)...)) {
      select({current_id});
      return;
    }
  }
  select({});
}

template <typename Container_T, typename Callback_T, typename... Arg_Ts>
void FindDialog::matchAll(const Container_T &container, Callback_T callback,
                          Arg_Ts &&... callback_params) {
  if (container.empty()) {
    return;
  }

  QString match_str = search_field->text();
  bool match_case = (match_case_checkbox->checkState() == Qt::Checked);
  bool full_match = (full_match_checkbox->checkState() == Qt::Checked);
  current_id = -1;
  std::vector<int> selected;

  for (size_t i = 0; i < container.size(); ++i) {
    if (callback(container[i], match_str, match_case, full_match,
                 std::forward<Arg_Ts>(callback_params)...)) {
      selected.push_back(i);
    }
  }
  select(selected);
}
