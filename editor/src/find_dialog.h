/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <QDialog>
#include <functional>
#include <vector>

class QPushButton;
class QLineEdit;
class QCheckBox;
class QListWidget;
class QVBoxLayout;

class FindDialog : public QDialog {
  Q_OBJECT

 public:
  virtual ~FindDialog() = default;
  bool event(QEvent *e) override;

 public slots:
  void findNext();
  void findPrev();
  virtual void selectAll() = 0;

 signals:
  void select(std::vector<int> selection);

 protected:
  void makeGui();
  virtual void addInputs(QVBoxLayout *inputs_layout) = 0;
  virtual void find(bool forward) = 0;
  template <typename Container_T, typename Callback_T, typename... Arg_Ts>
  void matchOne(const Container_T &container, bool forward, Callback_T callback,
                Arg_Ts &&... callback_params);
  template <typename Container_T, typename Callback_T, typename... Arg_Ts>
  void matchAll(const Container_T &container, Callback_T callback,
                Arg_Ts &&... callback_params);

 private:
  QLineEdit *search_field;
  QCheckBox *match_case_checkbox;
  QCheckBox *full_match_checkbox;
  int current_id = -1;
};

#include "find_dialog_impl.h"
