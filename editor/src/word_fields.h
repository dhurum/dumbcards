/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <QDialog>
#include <QGroupBox>
#include <list>
#include <vector>
#include "deck_info.h"

class QCheckBox;
class QPushButton;
class QGridLayout;
class QLineEdit;
class QLabel;

struct FieldInfo {
  QLineEdit *name;
  QCheckBox *is_key;
  QPushButton *remove_button;
  int64_t id;
};

class WordFields : public QGroupBox {
  Q_OBJECT

 public:
  WordFields(const DeckInfo *deck_info);
  std::vector<WordFieldInfo> getFields();
  bool checkCorrect();

 private:
  QGridLayout *fields_layout;
  std::list<FieldInfo> fields;
  QPushButton *add_field_button;
  QLabel *name_header;
  QLabel *key_header;
  bool editing = false;

  void addField(const WordFieldInfo *field_info);
  void deleteField(std::list<FieldInfo>::const_iterator it);

 private slots:
  void addFieldSlot();
};
