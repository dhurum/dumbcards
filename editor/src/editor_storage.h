/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <memory>
#include <vector>
#include "storage.h"

class Card;
struct Tag;

class EditorStorage : public Storage {
 public:
  EditorStorage(const QString &filename);
  EditorStorage(const QString &filename, const DeckInfo &deck_info);
  ~EditorStorage();
  void setDeckInfo(const DeckInfo &new_deck_info);
  std::vector<std::unique_ptr<Card>> allCards();
  void addCard(Card &card);
  void updateCard(const Card &card);
  void deleteCard(const Card &card);
  void addTag(Tag &tag);
  void updateTag(const Tag &tag);
  void deleteTag(const Tag &tag);

 private:
  std::unique_ptr<Sqlite::Stmt> add_deck_info;
  std::unique_ptr<Sqlite::Stmt> update_deck_info;
  std::unique_ptr<Sqlite::Stmt> add_word_field;
  std::unique_ptr<Sqlite::Stmt> update_word_field;
  std::unique_ptr<Sqlite::Stmt> get_all_cards;
  std::unique_ptr<Sqlite::Stmt> add_card_data;
  std::unique_ptr<Sqlite::Stmt> update_card_data;
  std::unique_ptr<Sqlite::Stmt> delete_card_data;
  std::unique_ptr<Sqlite::Stmt> add_card_instance;
  std::unique_ptr<Sqlite::Stmt> delete_card_instances;
  std::unique_ptr<Sqlite::Stmt> add_tag;
  std::unique_ptr<Sqlite::Stmt> update_tag;
  std::unique_ptr<Sqlite::Stmt> delete_tag;
  std::unique_ptr<Sqlite::Stmt> get_card_tags;
  std::unique_ptr<Sqlite::Stmt> add_card_tag;
  std::unique_ptr<Sqlite::Stmt> delete_card_tags_by_tag;
  std::unique_ptr<Sqlite::Stmt> delete_card_tags_by_card;

  void prepare();
};
