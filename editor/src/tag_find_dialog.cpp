/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "tag_find_dialog.h"
#include <QCheckBox>
#include <QLineEdit>
#include <QVBoxLayout>
#include "qt_helpers.h"

TagFindDialog::TagFindDialog(const std::vector<Tag> &tags) : tags(tags) {
  makeGui();
  setWindowTitle(tr("Find Tags"));
}

void TagFindDialog::addInputs(QVBoxLayout * /*inputs_layout*/) {}

static bool matchTag(const Tag &tag, const QString &match_str, bool match_case,
                     bool full_match) {
  return matchString(tag.name, match_str, match_case, full_match);
}

void TagFindDialog::find(bool forward) {
  matchOne(tags, forward, matchTag);
}

void TagFindDialog::selectAll() {
  matchAll(tags, matchTag);
}
