/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <memory>
#include <vector>
#include "find_dialog.h"
#include "tag.h"

class QCheckBox;
class QComboBox;
class Card;
class CardTagsSelector;

class CardFindDialog : public FindDialog {
  Q_OBJECT

 public:
  CardFindDialog(const std::vector<std::unique_ptr<Card>> &cards,
                 const std::vector<Tag> &tags);

 public slots:
  void selectAll() override;

 private:
  QCheckBox *match_all_checkbox;
  const std::vector<std::unique_ptr<Card>> &cards;
  const std::vector<Tag> &tags;
  CardTagsSelector *tags_selector;
  QComboBox *tags_type;
  int current_card = -1;

  void addInputs(QVBoxLayout *inputs_layout) override;
  void find(bool forward) override;
};
