/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "find_dialog.h"
#include <QCheckBox>
#include <QEvent>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include "qt_helpers.h"

void FindDialog::makeGui() {
  auto layout = new QHBoxLayout();
  setLayout(layout);

  auto inputs_layout = new QVBoxLayout();

  auto search_field_layout = new QHBoxLayout();
  search_field = new QLineEdit();
  search_field_layout->addWidget(new QLabel(tr("Find:")));
  search_field_layout->addWidget(search_field);
  inputs_layout->addLayout(search_field_layout);

  match_case_checkbox = new QCheckBox(tr("Match case"));
  inputs_layout->addWidget(match_case_checkbox);

  full_match_checkbox = new QCheckBox(tr("Match whole text"));
  inputs_layout->addWidget(full_match_checkbox);

  addInputs(inputs_layout);

  inputs_layout->addStretch();
  layout->addLayout(inputs_layout);

  auto buttons_layout = new QVBoxLayout();

  auto find_next_button = new QPushButton(tr("Find Next"));
  CONNECT(find_next_button, clicked, this, findNext);
  buttons_layout->addWidget(find_next_button);

  auto find_prev_button = new QPushButton(tr("Find Previous"));
  CONNECT(find_prev_button, clicked, this, findPrev);
  buttons_layout->addWidget(find_prev_button);

  auto select_all_button = new QPushButton(tr("Select All"));
  CONNECT(select_all_button, clicked, this, selectAll);
  buttons_layout->addWidget(select_all_button);

  auto close_button = new QPushButton(tr("Close"));
  CONNECT(close_button, clicked, this, close);
  buttons_layout->addWidget(close_button);

  buttons_layout->addStretch();
  layout->addLayout(buttons_layout);
}

void FindDialog::findNext() {
  find(true);
}

void FindDialog::findPrev() {
  find(false);
}

bool FindDialog::event(QEvent *e) {
  if (e->type() == QEvent::WindowActivate) {
    search_field->setFocus();
  }
  return QWidget::event(e);
}
