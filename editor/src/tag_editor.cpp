/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "tag_editor.h"
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>
#include "qt_helpers.h"

TagEditor::TagEditor(QWidget *parent, const Tag *tag) : QDialog(parent) {
  layout = new QVBoxLayout();
  setLayout(layout);

  auto form_layout = new QFormLayout();
  layout->addLayout(form_layout);

  name_field = new QLineEdit();
  form_layout->addRow(tr("Name:"), name_field);

  if (tag) {
    tag_id = tag->id;
    name_field->setText(tag->name);
    setWindowTitle(tr("Edit Tag"));
  } else {
    setWindowTitle(tr("New Tag"));
  }

  layout->addStretch();
  layout->addSpacing(20);

  auto button_box =
      new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
  layout->addWidget(button_box);

  auto ok_button = button_box->button(QDialogButtonBox::Ok);
  ok_button->setDefault(true);
  CONNECT(ok_button, clicked, this, check);
  auto cancel_button = button_box->button(QDialogButtonBox::Cancel);
  CONNECT(cancel_button, clicked, this, reject);
}

void TagEditor::check() {
  if (name_field->text().isEmpty()) {
    QMessageBox(QMessageBox::Warning, tr("Invalid Data"),
                tr("Please, set name"), QMessageBox::Ok, this)
        .exec();
    name_field->setFocus();
  } else {
    accept();
  }
}

Tag TagEditor::get() {
  return {tag_id, name_field->text()};
}
