/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "deck_editor.h"
#include <QCheckBox>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>
#include "qt_helpers.h"
#include "word_fields.h"

DeckEditor::DeckEditor(QWidget *parent, const DeckInfo *deck_info)
    : QDialog(parent) {
  auto layout = new QVBoxLayout();
  setLayout(layout);

  auto name_layout = new QHBoxLayout();
  layout->addLayout(name_layout);

  name_layout->addWidget(new QLabel(tr("Name:")));
  name_field = new QLineEdit();
  name_layout->addWidget(name_field);

  open_file_button = new QPushButton(tr("File"));
  CONNECT(open_file_button, clicked, this, chooseFile);
  layout->addWidget(open_file_button);

  if (deck_info) {
    name_field->setText(deck_info->name);
    open_file_button->hide();
    setWindowTitle(tr("Edit Deck"));
  } else {
    setWindowTitle(tr("New Deck"));
  }

  word_fields = new WordFields(deck_info);
  layout->addWidget(word_fields);
  layout->addStretch();

  auto button_box =
      new QDialogButtonBox(QDialogButtonBox::Save | QDialogButtonBox::Cancel);
  layout->addWidget(button_box);

  auto save_button = button_box->button(QDialogButtonBox::Save);
  save_button->setDefault(true);
  CONNECT(save_button, clicked, this, check);
  auto cancel_button = button_box->button(QDialogButtonBox::Cancel);
  CONNECT(cancel_button, clicked, this, reject);
}

void DeckEditor::chooseFile() {
  QString default_name("new_deck.dc");
  if (!name_field->text().isEmpty()) {
    default_name = name_field->text().toLower().replace(' ', '_');
    default_name.append(".dc");
  }

  filename = QFileDialog::getSaveFileName(
      this, tr("New Deck File"), default_name, tr("Dumbcards Deck (*.dc)"));
  if (filename.length()) {
    open_file_button->setText(filename);
  }
}

void DeckEditor::check() {
  if (name_field->text().isEmpty()) {
    QMessageBox(QMessageBox::Warning, tr("Invalid Data"),
                tr("Please, set name"), QMessageBox::Ok, this)
        .exec();
    name_field->setFocus();
    return;
  }
  if (open_file_button->isVisible() && filename.isEmpty()) {
    QMessageBox(QMessageBox::Warning, tr("Invalid Data"),
                tr("Please, set file"), QMessageBox::Ok, this)
        .exec();
    open_file_button->setFocus();
    return;
  }
  if (!word_fields->checkCorrect()) {
    return;
  }
  accept();
}

const QString &DeckEditor::getFilename() {
  return filename;
}

DeckInfo DeckEditor::getDeckInfo() {
  return {name_field->text(), word_fields->getFields()};
}
