/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "main_window.h"
#include <QAction>
#include <QFileDialog>
#include <QListWidget>
#include <QListWidgetItem>
#include <QMenuBar>
#include <QMessageBox>
#include <QModelIndex>
#include <QTabWidget>
#include <QToolBar>
#include "card.h"
#include "card_editor.h"
#include "card_find_dialog.h"
#include "deck_editor.h"
#include "deck_export.h"
#include "editor_storage.h"
#include "qt_helpers.h"
#include "tag_editor.h"
#include "tag_find_dialog.h"

MainWindow::MainWindow(const QString &deck_file) {
  setWindowTitle("Dumbcards Editor");
  auto deck_menu = menuBar()->addMenu(tr("&Deck"));

  auto action_new_deck = new QAction(tr("New"), this);
  action_new_deck->setStatusTip(tr("Create new deck"));
  CONNECT(action_new_deck, triggered, this, newDeck);
  deck_menu->addAction(action_new_deck);

  auto action_open_deck = new QAction(tr("Open"), this);
  action_open_deck->setShortcut(QKeySequence::Open);
  action_open_deck->setStatusTip(tr("Open deck"));
  CONNECT(action_open_deck, triggered, this, openDeck);
  deck_menu->addAction(action_open_deck);

  action_edit_deck = new QAction(tr("Edit"), this);
  action_edit_deck->setShortcut(Qt::CTRL + Qt::Key_E);
  action_edit_deck->setStatusTip(tr("Edit deck"));
  CONNECT(action_edit_deck, triggered, this, editDeck);
  deck_menu->addAction(action_edit_deck);
  action_edit_deck->setEnabled(false);

  action_export_deck = new QAction(tr("Export in JSON"), this);
  action_export_deck->setStatusTip(tr("Export deck in JSON"));
  CONNECT(action_export_deck, triggered, this, exportDeck);
  deck_menu->addAction(action_export_deck);
  action_export_deck->setEnabled(false);

  toolbar = addToolBar(tr("Card"));
  toolbar->setEnabled(false);

  action_new = new QAction(tr("Add"), this);
  action_new->setShortcuts(QKeySequence::New);
  toolbar->addAction(action_new);

  action_delete = new QAction(tr("Remove"), this);
  action_delete->setShortcuts(QKeySequence::Delete);
  toolbar->addAction(action_delete);

  action_find = new QAction(tr("Find"), this);
  action_find->setShortcuts(QKeySequence::Find);
  toolbar->addAction(action_find);

  tab_widget = new QTabWidget();
  CONNECT(tab_widget, currentChanged, this, tabChanged);
  setCentralWidget(tab_widget);
  tab_widget->setEnabled(false);

  cards_list = new QListWidget();
  cards_list->setSelectionMode(QAbstractItemView::ExtendedSelection);
  CONNECT(cards_list, activated, this, editCard);
  tab_widget->addTab(cards_list, tr("Cards"));

  tags_list = new QListWidget();
  tags_list->setSelectionMode(QAbstractItemView::ExtendedSelection);
  CONNECT(tags_list, activated, this, editTag);
  tab_widget->addTab(tags_list, tr("Tags"));

  openFile(deck_file);
}

MainWindow::~MainWindow() = default;

void MainWindow::tabChanged(int index) {
  disconnect(action_new_connection);
  disconnect(action_delete_connection);
  disconnect(action_find_connection);
  if (index == 0) {
    action_new->setStatusTip(tr("Create new card"));
    action_new_connection = CONNECT(action_new, triggered, this, newCard);

    action_delete->setStatusTip(tr("Delete card"));
    action_delete_connection =
        CONNECT(action_delete, triggered, this, deleteCard);

    action_find->setStatusTip(tr("Find card"));
    action_find_connection = CONNECT(action_find, triggered, this, findCard);
    if (tag_find_dialog) {
      tag_find_dialog->hide();
    }
  } else {
    action_new->setStatusTip(tr("Create new tag"));
    action_new_connection = CONNECT(action_new, triggered, this, newTag);

    action_delete->setStatusTip(tr("Delete tag"));
    action_delete_connection =
        CONNECT(action_delete, triggered, this, deleteTag);

    action_find->setStatusTip(tr("Find tag"));
    action_find_connection = CONNECT(action_find, triggered, this, findTag);
    if (card_find_dialog) {
      card_find_dialog->hide();
    }
  }
}

void MainWindow::readStorage() {
  cards = storage->allCards();
  cards_list->clear();
  for (const auto &card : cards) {
    cards_list->addItem((*card).getName());
  }
  tags = storage->getTags();
  tags_list->clear();
  for (const auto &tag : tags) {
    tags_list->addItem(tag.name);
  }
  card_find_dialog.reset();
  tag_find_dialog.reset();

  toolbar->setEnabled(true);
  tab_widget->setEnabled(true);
  updateTitle(storage->getDeckInfo().name);
}

void MainWindow::updateTitle(const QString &deck_name) {
  setWindowTitle(QString("%1 - Dumbcards Editor").arg(deck_name));
}

void MainWindow::newDeck() {
  DeckEditor editor(this);
  if (editor.exec() != QDialog::Accepted) {
    return;
  }
  storage.reset();
  try {
    storage = std::make_unique<EditorStorage>(editor.getFilename(),
                                              editor.getDeckInfo());
  } catch (...) {
    QFile::remove(editor.getFilename());
    throw;
  }
  readStorage();
  action_edit_deck->setEnabled(true);
  action_export_deck->setEnabled(true);
}

void MainWindow::openDeck() {
  openFile(QFileDialog::getOpenFileName(this, tr("Open Deck File"), "",
                                        tr("Dumbcards Deck (*.dc)")));
}

void MainWindow::editDeck() {
  DeckEditor editor(this, &(storage->getDeckInfo()));
  if (editor.exec() != QDialog::Accepted) {
    return;
  }
  storage->setDeckInfo(editor.getDeckInfo());
  updateTitle(storage->getDeckInfo().name);
}

void MainWindow::exportDeck() {
  QString default_name =
      storage->getDeckInfo().name.toLower().replace(' ', '_').append(".json");
  auto filename = QFileDialog::getSaveFileName(
      this, tr("Export To"), default_name, tr("JSON (*.json)"));
  if (filename.length()) {
    ::exportDeck(filename, storage->getDeckInfo(), cards, tags);
  }
}

void MainWindow::openFile(const QString &deck_file) {
  if (deck_file.isEmpty()) {
    return;
  }
  storage.reset();
  storage = std::make_unique<EditorStorage>(deck_file);
  readStorage();
  action_edit_deck->setEnabled(true);
  action_export_deck->setEnabled(true);
}

void MainWindow::showCardEditor(const QModelIndex &index) {
  Card *card = nullptr;
  if (index.isValid()) {
    card = cards[index.row()].get();
  }

  CardEditor editor(this, storage->getDeckInfo(), storage->getTags(), card);
  if (editor.exec() != QDialog::Accepted) {
    return;
  }

  if (index.isValid()) {
    cards[index.row()] = editor.get();
    cards_list->item(index.row())->setText(cards[index.row()]->getName());
    storage->updateCard(*cards[index.row()]);
  } else {
    cards.push_back(editor.get());
    storage->addCard(*cards.back());
    cards_list->addItem(cards.back()->getName());
  }
}

void MainWindow::newCard() {
  showCardEditor(QModelIndex());
}

void MainWindow::editCard(const QModelIndex &index) {
  showCardEditor(index);
}

void MainWindow::deleteCard() {
  std::vector<size_t> delete_indexes;
  auto selected_indexes = cards_list->selectionModel()->selectedIndexes();
  if (selected_indexes.isEmpty()) {
    return;
  }

  auto ret = QMessageBox(QMessageBox::Question, tr("Delete Cards"),
                         tr("Are you sure you want to delete selected cards?"),
                         QMessageBox::Yes | QMessageBox::No, this)
                 .exec();
  if (ret != QMessageBox::Yes) {
    return;
  }

  for (const auto &selected : selected_indexes) {
    storage->deleteCard(*cards[selected.row()]);
    delete_indexes.push_back(selected.row());
  }
  std::sort(delete_indexes.begin(), delete_indexes.end(),
            [](int a, int b) { return a > b; });

  for (auto i : delete_indexes) {
    cards.erase(cards.begin() + i);
    cards_list->takeItem(i);
  }
}

void MainWindow::findCard() {
  if (!card_find_dialog) {
    card_find_dialog = std::make_unique<CardFindDialog>(cards, tags);
    CONNECT(card_find_dialog.get(), select, this, selectCards);
  }
  card_find_dialog->show();
  card_find_dialog->raise();
  card_find_dialog->activateWindow();
}

void MainWindow::selectCards(std::vector<int> selection) {
  cards_list->clearSelection();
  if (selection.empty()) {
    return;
  }
  for (auto i : selection) {
    cards_list->item(i)->setSelected(true);
  }
  cards_list->scrollToItem(cards_list->item(selection[0]));
}

void MainWindow::onTagsChange() {
  card_find_dialog.reset();
}

void MainWindow::showTagEditor(const QModelIndex &index) {
  Tag *tag = nullptr;
  if (index.isValid()) {
    tag = &tags[index.row()];
  }

  TagEditor editor(this, tag);
  if (editor.exec() != QDialog::Accepted) {
    return;
  }

  if (index.isValid()) {
    tags[index.row()] = editor.get();
    tags_list->item(index.row())->setText(tags[index.row()].name);
    storage->updateTag(tags[index.row()]);
  } else {
    tags.push_back(editor.get());
    storage->addTag(tags.back());
    tags_list->addItem(tags.back().name);
  }
  onTagsChange();
}

void MainWindow::newTag() {
  showTagEditor(QModelIndex());
}

void MainWindow::editTag(const QModelIndex &index) {
  showTagEditor(index);
}

void MainWindow::deleteTag() {
  std::vector<size_t> delete_indexes;
  auto selected_indexes = tags_list->selectionModel()->selectedIndexes();
  if (selected_indexes.isEmpty()) {
    return;
  }

  auto ret = QMessageBox(QMessageBox::Question, tr("Delete Tags"),
                         tr("Are you sure you want to delete selected tags?"),
                         QMessageBox::Yes | QMessageBox::No, this)
                 .exec();
  if (ret != QMessageBox::Yes) {
    return;
  }

  for (const auto selected : selected_indexes) {
    storage->deleteTag(tags[selected.row()]);
    delete_indexes.push_back(selected.row());
  }
  std::sort(delete_indexes.begin(), delete_indexes.end(),
            [](int a, int b) { return a > b; });

  for (auto i : delete_indexes) {
    tags.erase(tags.begin() + i);
    tags_list->takeItem(i);
  }
  onTagsChange();
}

void MainWindow::findTag() {
  if (!tag_find_dialog) {
    tag_find_dialog = std::make_unique<TagFindDialog>(tags);
    CONNECT(tag_find_dialog.get(), select, this, selectTags);
  }
  tag_find_dialog->show();
  tag_find_dialog->raise();
  tag_find_dialog->activateWindow();
}

void MainWindow::selectTags(std::vector<int> selection) {
  tags_list->clearSelection();
  if (selection.empty()) {
    return;
  }
  for (auto i : selection) {
    tags_list->item(i)->setSelected(true);
  }
  tags_list->scrollToItem(tags_list->item(selection[0]));
}

void MainWindow::closeEvent(QCloseEvent * /*event*/) {
  card_find_dialog.reset();
  tag_find_dialog.reset();
}
