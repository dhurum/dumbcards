/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "word_fields.h"
#include <QCheckBox>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>
#include "qt_helpers.h"

WordFields::WordFields(const DeckInfo *deck_info)
    : QGroupBox(tr("Word fields")) {
  auto layout = new QVBoxLayout(this);
  add_field_button = new QPushButton(tr("Add field"));
  CONNECT(add_field_button, clicked, this, addFieldSlot);
  layout->addWidget(add_field_button);

  fields_layout = new QGridLayout();
  layout->addLayout(fields_layout);
  name_header = new QLabel(tr("Name"));
  fields_layout->addWidget(name_header, 0, 0);
  name_header->hide();
  fields_layout->itemAtPosition(0, 0)->setAlignment(Qt::AlignHCenter);
  key_header = new QLabel(tr("Key"));
  fields_layout->addWidget(key_header, 0, 1);
  key_header->hide();

  if (deck_info) {
    for (const auto &field_info : deck_info->word_fields) {
      addField(&field_info);
    }
    editing = true;
  }
}

void WordFields::addFieldSlot() {
  addField(nullptr);
}

void WordFields::addField(const WordFieldInfo *field_info) {
  FieldInfo field{new QLineEdit(), new QCheckBox(),
                  new QPushButton(tr("Remove")), 0};
  if (field_info) {
    field.name->setText(field_info->name);
    field.is_key->setCheckState(field_info->is_key ? Qt::Checked
                                                   : Qt::Unchecked);
    field.id = field_info->id;
    field.remove_button->setEnabled(false);
  }
  if (field_info || editing) {
    field.is_key->setEnabled(false);
  }

  size_t row = fields_layout->rowCount();

  fields_layout->addWidget(field.name, row, 0);
  field.name->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
  fields_layout->addWidget(field.is_key, row, 1);
  fields_layout->addWidget(field.remove_button, row, 2);

  if (fields.empty()) {
    name_header->show();
    key_header->show();
    setTabOrder(add_field_button, field.name);
  } else {
    setTabOrder(fields.back().remove_button, field.name);
  }
  setTabOrder(field.name, field.is_key);
  setTabOrder(field.is_key, field.remove_button);

  fields.push_back(field);
  auto it = --fields.end();
  CONNECT_FUNCTION(field.remove_button, clicked,
                   ([this, it]() { deleteField(it); }));
}

void WordFields::deleteField(std::list<FieldInfo>::const_iterator it) {
  fields_layout->removeWidget(it->name);
  delete it->name;
  fields_layout->removeWidget(it->is_key);
  delete it->is_key;
  fields_layout->removeWidget(it->remove_button);
  delete it->remove_button;

  fields.erase(it);

  if (fields.empty()) {
    name_header->hide();
    key_header->hide();
  }
}

std::vector<WordFieldInfo> WordFields::getFields() {
  std::vector<WordFieldInfo> word_fields;

  for (auto &field : fields) {
    word_fields.push_back(
        {field.name->text(),
         (field.is_key->checkState() == Qt::Checked) ? true : false, field.id});
  }

  return word_fields;
}

bool WordFields::checkCorrect() {
  bool key_exists = false;

  for (auto &field : fields) {
    if (field.name->text().isEmpty()) {
      QMessageBox(QMessageBox::Warning, tr("Invalid Data"),
                  tr("Please, set field name"), QMessageBox::Ok, this)
          .exec();
      field.name->setFocus();
      return false;
    }
    if (field.is_key->checkState() == Qt::Checked) {
      key_exists = true;
    }
  }
  if (!fields.empty()) {
    if (!key_exists) {
      QMessageBox(QMessageBox::Warning, tr("Invalid Data"),
                  tr("Please, mark at least one field as a key"),
                  QMessageBox::Ok, this)
          .exec();
      fields.front().is_key->setFocus();
      return false;
    }
    if (fields.size() == 1) {
      QMessageBox(
          QMessageBox::Warning, tr("Invalid Data"),
          tr("Please, add at least one more field or remove existing one"),
          QMessageBox::Ok, this)
          .exec();
      add_field_button->setFocus();
      return false;
    }
  }
  return true;
}
