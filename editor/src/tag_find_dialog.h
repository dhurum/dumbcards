/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <memory>
#include <vector>
#include "find_dialog.h"
#include "tag.h"

class TagFindDialog : public FindDialog {
  Q_OBJECT

 public:
  TagFindDialog(const std::vector<Tag> &tags);

 public slots:
  void selectAll() override;

 private:
  const std::vector<Tag> &tags;
  int current_tag = -1;

  void addInputs(QVBoxLayout *inputs_layout) override;
  void find(bool forward) override;
};
