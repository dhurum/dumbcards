/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "fact_card_editor.h"
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QTextEdit>
#include <QVBoxLayout>
#include "fact_card.h"
#include "qt_helpers.h"

FactCardEditor::FactCardEditor(const Card *_card) {
  layout = new QVBoxLayout();
  setLayout(layout);

  auto title_layout = new QHBoxLayout();
  title_layout->addWidget(new QLabel(tr("Title:")));
  title = new QLineEdit();
  title_layout->addWidget(title);
  layout->addLayout(title_layout);

  layout->addWidget(new QLabel(tr("Description:")));
  description = new QTextEdit();
  description->setTabChangesFocus(true);
  layout->addWidget(description);

  if (_card && (_card->getType() == Card::TypeFact)) {
    const auto card = dynamic_cast<const FactCard *>(_card);
    title->setText(card->getName());
    description->setText(card->getDescription());
  }
}

std::unique_ptr<Card> FactCardEditor::get(int64_t card_id,
                                          const std::vector<int64_t> &tags) {
  return std::make_unique<FactCard>(card_id, title->text(),
                                    description->toPlainText(), tags);
}

bool FactCardEditor::checkCorrect() {
  if (title->text().isEmpty()) {
    QMessageBox(QMessageBox::Warning, tr("Invalid Data"),
                tr("Please, fill title"), QMessageBox::Ok, this)
        .exec();
    title->setFocus();
    return false;
  }

  if (description->toPlainText().isEmpty()) {
    QMessageBox(QMessageBox::Warning, tr("Invalid Data"),
                tr("Please, fill description"), QMessageBox::Ok, this)
        .exec();
    description->setFocus();
    return false;
  }

  return true;
}
