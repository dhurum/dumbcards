/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <QDialog>
#include <memory>
#include <string>
#include <vector>
#include "tag.h"

struct DeckInfo;
class QComboBox;
class QPushButton;
class WordCardEditor;
class TableCardEditor;
class FactCardEditor;
class CommonCardEditor;
class CardTagsSelector;
class Card;

class CardEditor : public QDialog {
  Q_OBJECT

 public:
  CardEditor(QWidget *parent, const DeckInfo &deck_info,
             const std::vector<Tag> &tags, const Card *card = nullptr);
  ~CardEditor();
  std::unique_ptr<Card> get();

 private:
  QComboBox *editor_picker;
  WordCardEditor *word_editor;
  TableCardEditor *table_editor;
  FactCardEditor *fact_editor;
  CommonCardEditor *current_editor;
  CardTagsSelector *tags_selector;
  int64_t card_id = 0;

  void showWord();
  void showTable();
  void showFact();

 private slots:
  void setEditorType(int index);
  void check();
};
