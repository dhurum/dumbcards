/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "table_card_editor.h"
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QTextEdit>
#include <QVBoxLayout>
#include "qt_helpers.h"
#include "table_card.h"

TableCardEditor::TableCardEditor(const Card *_card) {
  layout = new QVBoxLayout();
  setLayout(layout);

  auto title_layout = new QHBoxLayout();
  title_layout->addWidget(new QLabel(tr("Title:")));
  title = new QLineEdit();
  title_layout->addWidget(title);
  layout->addLayout(title_layout);

  auto table_controls_layout = new QGridLayout();
  auto add_row = new QPushButton(tr("Add Row"));
  CONNECT(add_row, clicked, this, addRow);
  table_controls_layout->addWidget(add_row, 0, 0);

  auto delete_row = new QPushButton(tr("Delete Row"));
  CONNECT(delete_row, clicked, this, deleteRow);
  table_controls_layout->addWidget(delete_row, 0, 1);

  auto add_column = new QPushButton(tr("Add Column"));
  CONNECT(add_column, clicked, this, addColumn);
  table_controls_layout->addWidget(add_column, 1, 0);

  auto delete_column = new QPushButton(tr("Delete Column"));
  CONNECT(delete_column, clicked, this, deleteColumn);
  table_controls_layout->addWidget(delete_column, 1, 1);
  layout->addLayout(table_controls_layout);

  table_layout = new QGridLayout();
  layout->addLayout(table_layout);
  addRow();

  layout->addWidget(new QLabel(tr("Appendix:")));
  appendix = new QTextEdit();
  appendix->setTabChangesFocus(true);
  layout->addWidget(appendix);

  if (_card && (_card->getType() == Card::TypeTable)) {
    const auto card = dynamic_cast<const TableCard *>(_card);

    title->setText(card->getName());

    while (table_rows < card->rowsNum()) {
      addRow();
    }
    while (table_cols < card->colsNum()) {
      addColumn();
    }

    for (size_t i = 0; i < table_rows; ++i) {
      for (size_t j = 0; j < table_cols; ++j) {
        table[i][j]->setText(card->getCell(i, j));
      }
    }

    appendix->setText(card->getAppendix());
  }
}

void TableCardEditor::addRow() {
  table_rows += 1;
  table.resize(table_rows);

  for (size_t i = 0; i < table_cols; ++i) {
    auto cell = new QLineEdit();
    cell->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    table_layout->addWidget(cell, table_rows - 1, i);
    if (i == 0) {
      if (table.size() > 1) {
        setTabOrder(table[table.size() - 2].back(), cell);
      }
    } else {
      setTabOrder(table.back().back(), cell);
    }
    table.back().push_back(cell);
  }
}

void TableCardEditor::deleteRow() {
  if (table_rows == 1) {
    return;
  }
  for (auto cell : table.back()) {
    table_layout->removeWidget(cell);
    delete cell;
  }
  table_rows -= 1;
  table.resize(table_rows);
}

void TableCardEditor::addColumn() {
  table_cols += 1;

  for (size_t i = 0; i < table_rows; ++i) {
    auto cell = new QLineEdit();
    cell->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    table_layout->addWidget(cell, i, table_cols - 1);
    setTabOrder(table[i].back(), cell);
    table[i].push_back(cell);
  }
}

void TableCardEditor::deleteColumn() {
  if (table_cols == 1) {
    return;
  }
  for (auto &row : table) {
    table_layout->removeWidget(row.back());
    delete row.back();
    row.pop_back();
  }
  table_cols -= 1;
}

std::unique_ptr<Card> TableCardEditor::get(int64_t card_id,
                                           const std::vector<int64_t> &tags) {
  std::vector<std::vector<QString>> cells(table.size());

  for (size_t i = 0; i < table.size(); ++i) {
    for (auto cell : table[i]) {
      cells[i].push_back(cell->text());
    }
  }
  return std::make_unique<TableCard>(card_id, title->text(), cells,
                                     appendix->toPlainText(), tags);
}

bool TableCardEditor::checkCorrect() {
  if (title->text().isEmpty()) {
    QMessageBox(QMessageBox::Warning, tr("Invalid Data"),
                tr("Please, fill title"), QMessageBox::Ok, this)
        .exec();
    title->setFocus();
    return false;
  }
  int filled = 0;

  for (const auto &row : table) {
    for (auto cell : row) {
      if (!cell->text().isEmpty()) {
        ++filled;
      }
    }
  }
  if (filled >= 1) {
    return true;
  }
  QMessageBox(QMessageBox::Warning, tr("Invalid Data"),
              tr("Please, fill at least one cell"), QMessageBox::Ok, this)
      .exec();
  table[0][0]->setFocus();
  return false;
}
