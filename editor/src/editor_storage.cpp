/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "editor_storage.h"
#include <QFile>
#include <QObject>
#include <stdexcept>
#include "card.h"
#include "fact_card.h"
#include "sqlite_wrapper.h"
#include "table_card.h"
#include "tag.h"
#include "word_card.h"

EditorStorage::EditorStorage(const QString &filename) : Storage(filename) {
  prepare();
}

EditorStorage::EditorStorage(const QString &filename,
                             const DeckInfo &_deck_info) {
  if (QFile::exists(filename)) {
    if (!QFile::remove(filename)) {
      throw std::runtime_error(QObject::tr("Can't remove existing file '%1'")
                                   .arg(filename)
                                   .toStdString());
    }
  }

  db = std::make_unique<Sqlite::DB>(filename.toStdString());
  db->exec("create table deck_info(name text, use_words integer)");
  db->exec("create table word_field_info(name text, is_key integer)");
  db->exec("create table card_data(type integer, data blob)");
  db->exec(
      "create table card_instance(data_id integer, field integer, "
      "correct integer, last_answered text)");
  db->exec("create table tag(name text)");
  db->exec("create table card_tag(data_id integer, tag_id integer)");

  Storage::prepare();
  prepare();

  deck_info = _deck_info;
  add_deck_info->bind(":name", deck_info.name.toStdString());
  add_deck_info->exec();

  if (!deck_info.word_fields.empty()) {
    for (auto &field : deck_info.word_fields) {
      add_word_field->bind(":name", field.name.toStdString());
      add_word_field->bind(":is_key", field.is_key);
      add_word_field->exec();
      field.id = db->lastInsertRowid();
    }
  }
}

EditorStorage::~EditorStorage() = default;

void EditorStorage::prepare() {
  add_deck_info = db->prepare("insert into deck_info (name) values(:name)");
  update_deck_info = db->prepare("update deck_info set name = :name");
  add_word_field = db->prepare(
      "insert into word_field_info (name, is_key) values (:name, :is_key)");
  update_word_field = db->prepare(
      "update word_field_info set name = :name where rowid = :rowid");
  get_all_cards = db->prepare("select rowid, type, data from card_data");
  add_card_data =
      db->prepare("insert into card_data (type, data) values (:type, :data)");
  update_card_data =
      db->prepare("update card_data set data = :data where rowid = :rowid");
  delete_card_data = db->prepare("delete from card_data where rowid = :rowid");
  add_card_instance = db->prepare(
      "insert into card_instance (data_id, field, correct, last_answered) "
      "values (:data_id, :field, 0, datetime('now'))");
  delete_card_instances =
      db->prepare("delete from card_instance where data_id = :data_id");

  add_tag = db->prepare("insert into tag values (:name)");
  update_tag = db->prepare("update tag set name = :name where rowid = :rowid");
  delete_tag = db->prepare("delete from tag where rowid = :rowid");

  get_card_tags = db->prepare(
      "select tag_id from card_tag where data_id = :data_id order by tag_id");
  add_card_tag = db->prepare("insert into card_tag values (:data_id, :tag_id)");
  delete_card_tags_by_tag =
      db->prepare("delete from card_tag where tag_id = :tag_id");
  delete_card_tags_by_card =
      db->prepare("delete from card_tag where data_id = :data_id");
}

void EditorStorage::setDeckInfo(const DeckInfo &new_deck_info) {
  db->transaction();

  if (deck_info.name != new_deck_info.name) {
    update_deck_info->bind(":name", new_deck_info.name.toStdString());
    update_deck_info->exec();
    deck_info.name = new_deck_info.name;
  }
  auto &fields = deck_info.word_fields;
  auto &new_fields = new_deck_info.word_fields;

  for (size_t i = 0; i < new_fields.size(); ++i) {
    if (i >= fields.size()) {
      WordFieldInfo new_field = {new_fields[i].name, false, 0};

      add_word_field->bind(":name", new_field.name.toStdString());
      add_word_field->bind(":is_key", false);
      add_word_field->exec();

      new_field.id = db->lastInsertRowid();
      fields.push_back(new_field);

    } else if (fields[i].name != new_fields[i].name) {
      update_word_field->bind(":name", new_fields[i].name.toStdString());
      update_word_field->bind(":rowid", new_fields[i].id);
      update_word_field->exec();

      fields[i].name = new_fields[i].name;
    }
  }
  db->commit();
}

std::vector<std::unique_ptr<Card>> EditorStorage::allCards() {
  std::vector<std::unique_ptr<Card>> cards;

  get_all_cards->exec();
  while (get_all_cards->next()) {
    auto data_id = get_all_cards->get<int64_t>(0);

    get_card_tags->bind(":data_id", data_id);
    get_card_tags->exec();

    std::vector<int64_t> tags;
    while (get_card_tags->next()) {
      tags.push_back(get_card_tags->get<int64_t>(0));
    }

    auto data = get_all_cards->get<std::pair<const void *, size_t>>(2);
    std::string data_str(static_cast<const char *>(data.first), data.second);
    int type = get_all_cards->get<int>(1);
    switch (type) {
      case Card::TypeWord:
        cards.push_back(std::make_unique<WordCard>(data_id, data_str, tags));
        break;
      case Card::TypeTable:
        cards.push_back(std::make_unique<TableCard>(data_id, data_str, tags));
        break;
      case Card::TypeFact:
        cards.push_back(std::make_unique<FactCard>(data_id, data_str, tags));
        break;
    }
  }
  return cards;
}

void EditorStorage::addCard(Card &card) {
  db->transaction();

  auto data_str = card.serialize();

  add_card_data->bind(":type", card.getType());
  add_card_data->bind(":data", data_str);
  add_card_data->exec();

  setCardId(card, db->lastInsertRowid());

  add_card_instance->bind(":data_id", card.getId());

  if (card.getType() == Card::TypeWord) {
    for (int i = 0; i < static_cast<int>(deck_info.word_fields.size()); ++i) {
      if (deck_info.word_fields[i].is_key) {
        add_card_instance->bind(":field", i);
        add_card_instance->exec();
      }
    }
  } else {
    add_card_instance->bind(":field", 0);
    add_card_instance->exec();
  }
  add_card_tag->bind(":data_id", card.getId());
  for (auto tag : card.getTags()) {
    add_card_tag->bind(":tag_id", tag);
    add_card_tag->exec();
  }
  db->commit();
}

void EditorStorage::updateCard(const Card &card) {
  db->transaction();

  auto data_str = card.serialize();

  update_card_data->bind(":data", data_str);
  update_card_data->bind(":rowid", card.getId());
  update_card_data->exec();

  delete_card_tags_by_card->bind(":data_id", card.getId());
  delete_card_tags_by_card->exec();

  add_card_tag->bind(":data_id", card.getId());
  for (auto tag : card.getTags()) {
    add_card_tag->bind(":tag_id", tag);
    add_card_tag->exec();
  }

  db->commit();
}

void EditorStorage::deleteCard(const Card &card) {
  db->transaction();

  delete_card_data->bind(":rowid", card.getId());
  delete_card_data->exec();

  delete_card_instances->bind(":data_id", card.getId());
  delete_card_instances->exec();

  delete_card_tags_by_card->bind(":data_id", card.getId());
  delete_card_tags_by_card->exec();

  db->commit();
}

void EditorStorage::addTag(Tag &tag) {
  add_tag->bind(":name", tag.name.toStdString());
  add_tag->exec();
  tag.id = db->lastInsertRowid();
}

void EditorStorage::updateTag(const Tag &tag) {
  update_tag->bind(":name", tag.name.toStdString());
  update_tag->bind(":rowid", tag.id);
  update_tag->exec();
}

void EditorStorage::deleteTag(const Tag &tag) {
  db->transaction();

  delete_tag->bind(":rowid", tag.id);
  delete_tag->exec();

  delete_card_tags_by_tag->bind(":tag_id", tag.id);
  delete_card_tags_by_tag->exec();

  db->commit();
}
