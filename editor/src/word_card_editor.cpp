/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "word_card_editor.h"
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include "deck_info.h"
#include "word_card.h"

WordCardEditor::WordCardEditor(const DeckInfo &deck_info, const Card *_card)
    : deck_info(deck_info) {
  auto layout = new QFormLayout();
  setLayout(layout);

  for (const auto &field : deck_info.word_fields) {
    fields.push_back(new QLineEdit());
    layout->addRow(QString(field.name).append(":"), fields.back());
  }
  if (_card && (_card->getType() == Card::TypeWord)) {
    const auto card = dynamic_cast<const WordCard *>(_card);

    for (size_t i = 0; i < std::min(fields.size(), card->fieldsNum()); ++i) {
      fields[i]->setText(card->getField(i));
    }
  }
}

std::unique_ptr<Card> WordCardEditor::get(int64_t card_id,
                                          const std::vector<int64_t> &tags) {
  std::vector<QString> values;
  for (auto field : fields) {
    values.push_back(field->text());
  }
  return std::make_unique<WordCard>(card_id, values, tags);
}

bool WordCardEditor::checkCorrect() {
  int filled = 0;

  for (size_t i = 0; i < deck_info.word_fields.size(); ++i) {
    if (fields[i]->text().isEmpty()) {
      if (!deck_info.word_fields[i].is_key) {
        continue;
      }
      QMessageBox(QMessageBox::Warning, tr("Invalid Data"),
                  tr("Please, fill key fields"), QMessageBox::Ok, this)
          .exec();
      fields[i]->setFocus();
      return false;
    } else {
      ++filled;
    }
  }
  if (filled >= 2) {
    return true;
  }
  QMessageBox(QMessageBox::Warning, tr("Invalid Data"),
              tr("Please, fill at least two fields"), QMessageBox::Ok, this)
      .exec();
  fields[0]->setFocus();
  return false;
}
