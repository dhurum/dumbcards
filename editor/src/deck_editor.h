/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <QDialog>
#include "deck_info.h"

class QPushButton;
class QLineEdit;
class WordFields;

class DeckEditor : public QDialog {
  Q_OBJECT

 public:
  DeckEditor(QWidget *parent, const DeckInfo *deck_info = nullptr);
  const QString &getFilename();
  DeckInfo getDeckInfo();

 private:
  QLineEdit *name_field;
  QPushButton *open_file_button;
  QString filename;
  WordFields *word_fields;

 private slots:
  void chooseFile();
  void check();
};
