/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "card_editor.h"
#include <QComboBox>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QVBoxLayout>
#include "card.h"
#include "card_tags_selector.h"
#include "common_card_editor.h"
#include "deck_info.h"
#include "fact_card_editor.h"
#include "qt_helpers.h"
#include "table_card_editor.h"
#include "word_card_editor.h"

CardEditor::CardEditor(QWidget *parent, const DeckInfo &deck_info,
                       const std::vector<Tag> &tags, const Card *card)
    : QDialog(parent) {
  auto layout = new QVBoxLayout();
  setLayout(layout);

  editor_picker = new QComboBox();
  editor_picker->addItem(tr("Word"));
  editor_picker->addItem(tr("Table"));
  editor_picker->addItem(tr("Fact"));
  CONNECT_OVERLOAD(editor_picker, currentIndexChanged, this, setEditorType,
                   void, (int));
  layout->addWidget(editor_picker);

  if (card) {
    card_id = card->getId();
  }

  word_editor = new WordCardEditor(deck_info, card);
  layout->addWidget(word_editor);

  table_editor = new TableCardEditor(card);
  layout->addWidget(table_editor);

  fact_editor = new FactCardEditor(card);
  layout->addWidget(fact_editor);

  showWord();
  setWindowTitle(tr("New Card"));

  if (card) {
    if (card->getType() == Card::TypeTable) {
      editor_picker->setCurrentIndex(1);
    } else if (card->getType() == Card::TypeFact) {
      editor_picker->setCurrentIndex(2);
    }
    setWindowTitle(tr("Edit Card"));
    editor_picker->setEnabled(false);
  } else if (deck_info.word_fields.empty()) {
    showTable();
  }

  layout->addWidget(new QLabel(tr("Tags:")));
  tags_selector = new CardTagsSelector(tags, card ? &card->getTags() : nullptr);
  layout->addWidget(tags_selector);

  layout->addStretch();
  layout->addSpacing(20);

  auto button_box =
      new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
  layout->addWidget(button_box);

  auto ok_button = button_box->button(QDialogButtonBox::Ok);
  ok_button->setDefault(true);
  CONNECT(ok_button, clicked, this, check);
  auto cancel_button = button_box->button(QDialogButtonBox::Cancel);
  CONNECT(cancel_button, clicked, this, reject);
}

CardEditor::~CardEditor() = default;

void CardEditor::setEditorType(int index) {
  if (index == 0) {
    showWord();
  } else if (index == 1) {
    showTable();
  } else {
    showFact();
  }
}

void CardEditor::check() {
  if (current_editor->checkCorrect()) {
    accept();
  }
}

void CardEditor::showWord() {
  word_editor->show();
  table_editor->hide();
  fact_editor->hide();
  current_editor = word_editor;
}

void CardEditor::showTable() {
  word_editor->hide();
  table_editor->show();
  fact_editor->hide();
  current_editor = table_editor;
}

void CardEditor::showFact() {
  word_editor->hide();
  table_editor->hide();
  fact_editor->show();
  current_editor = fact_editor;
}

std::unique_ptr<Card> CardEditor::get() {
  return current_editor->get(card_id, tags_selector->get());
}
