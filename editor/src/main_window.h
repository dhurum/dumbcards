/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <QMainWindow>
#include <memory>
#include <vector>
#include "tag.h"

class QListWidgetItem;
class QListWidget;
class QAction;
class QToolBar;
class QTabWidget;
class EditorStorage;
class Card;
class FindDialog;

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  MainWindow(const QString &deck_file);
  ~MainWindow();

 private:
  QToolBar *toolbar;
  QAction *action_new;
  QMetaObject::Connection action_new_connection;
  QAction *action_delete;
  QMetaObject::Connection action_delete_connection;
  QAction *action_find;
  QMetaObject::Connection action_find_connection;
  QAction *action_edit_deck;
  QAction *action_export_deck;
  QTabWidget *tab_widget;
  QListWidget *cards_list;
  QListWidget *tags_list;
  std::unique_ptr<FindDialog> card_find_dialog;
  std::unique_ptr<FindDialog> tag_find_dialog;
  std::unique_ptr<EditorStorage> storage;
  std::vector<std::unique_ptr<Card>> cards;
  std::vector<Tag> tags;

  void updateTitle(const QString &deck_name);
  void readStorage();
  void showCardEditor(const QModelIndex &index);
  void openFile(const QString &deck_file);
  void onTagsChange();
  void showTagEditor(const QModelIndex &index);
  void closeEvent(QCloseEvent *event);

 private slots:
  void tabChanged(int index);
  void newDeck();
  void openDeck();
  void editDeck();
  void exportDeck();
  void newCard();
  void editCard(const QModelIndex &index);
  void deleteCard();
  void findCard();
  void selectCards(std::vector<int> selection);
  void newTag();
  void editTag(const QModelIndex &index);
  void deleteTag();
  void findTag();
  void selectTags(std::vector<int> selection);
};
