
/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "deck_export.h"
#include <stdio.h>
#include <yajl/yajl_gen.h>
#include <sstream>
#include <stdexcept>
#include "card.h"
#include "deck_info.h"
#include "fact_card.h"
#include "table_card.h"
#include "word_card.h"

class JsonWriter {
 public:
  JsonWriter(const std::string &filename);
  ~JsonWriter();
  void write(const char *value);
  void writeName(const char *value);
  void write(const std::string &value);
  void write(int64_t value);
  void write(bool value);
  void write(const char *name, const char *value);
  void write(const char *name, const std::string &value);
  void write(const char *name, int64_t value);
  void write(const char *name, bool value);
  void startArray(const char *name);
  void startArray();
  void endArray();
  void startObject(const char *name);
  void startObject();
  void endObject();

 private:
  FILE *fd;
  yajl_gen generator;

  bool _write(const char *value);
  bool _write(const std::string &value);
};

static int jsonPrintCallback(void *ctx, const char *str, size_t len) {
  auto fd = *static_cast<FILE **>(ctx);

  if (fwrite(str, 1, len, fd) != len) {
    std::ostringstream err;
    err << "Can't write to file: " << strerror(errno);
    throw std::runtime_error(err.str());
  }
  return 0;
}

JsonWriter::JsonWriter(const std::string &filename) {
  fd = fopen(filename.c_str(), "w");
  if (!fd) {
    std::ostringstream err;
    err << "Can't open file '" << filename << "': " << strerror(errno);
    throw std::runtime_error(err.str());
  }

  generator = yajl_gen_alloc(nullptr);
  if (!generator) {
    throw std::runtime_error("Can't init yajl generator");
  }
  if (!yajl_gen_config(generator, yajl_gen_beautify, 1)) {
    throw std::runtime_error("Can't config yajl generator: beautify");
  }
  if (!yajl_gen_config(generator, yajl_gen_validate_utf8, 1)) {
    throw std::runtime_error("Can't config yajl generator: validate_utf8");
  }
  if (!yajl_gen_config(generator, yajl_gen_print_callback, jsonPrintCallback,
                       &fd)) {
    throw std::runtime_error("Can't config yajl generator: print_callback");
  }
}

JsonWriter::~JsonWriter() {
  yajl_gen_free(generator);
  fclose(fd);
}

bool JsonWriter::_write(const char *value) {
  return yajl_gen_status_ok
         == yajl_gen_string(generator,
                            reinterpret_cast<const unsigned char *>(value),
                            strlen(value));
}

bool JsonWriter::_write(const std::string &value) {
  return yajl_gen_status_ok
         == yajl_gen_string(generator, reinterpret_cast<const unsigned char *>(
                                           value.c_str()),
                            value.length());
}

void JsonWriter::write(const char *value) {
  if (!_write(value)) {
    std::ostringstream err;
    err << "Can't write string '" << value << "' in json";
    throw std::runtime_error(err.str());
  }
}

void JsonWriter::writeName(const char *value) {
  if (!_write(value)) {
    std::ostringstream err;
    err << "Can't write name '" << value << "' in json";
    throw std::runtime_error(err.str());
  }
}

void JsonWriter::write(const std::string &value) {
  if (!_write(value)) {
    std::ostringstream err;
    err << "Can't write string '" << value << "' in json";
    throw std::runtime_error(err.str());
  }
}

void JsonWriter::write(int64_t value) {
  if (yajl_gen_status_ok != yajl_gen_integer(generator, value)) {
    std::ostringstream err;
    err << "Can't write integer '" << value << "' in json";
    throw std::runtime_error(err.str());
  }
}

void JsonWriter::write(bool value) {
  if (yajl_gen_status_ok != yajl_gen_bool(generator, value)) {
    std::ostringstream err;
    err << "Can't write boolean '" << value << "' in json";
    throw std::runtime_error(err.str());
  }
}

void JsonWriter::write(const char *name, const char *value) {
  writeName(name);
  if (!_write(value)) {
    std::ostringstream err;
    err << "Can't write string '" << name << "': '" << value << "' in json";
    throw std::runtime_error(err.str());
  }
}

void JsonWriter::write(const char *name, const std::string &value) {
  writeName(name);
  if (!_write(value)) {
    std::ostringstream err;
    err << "Can't write string '" << name << "': '" << value << "' in json";
    throw std::runtime_error(err.str());
  }
}

void JsonWriter::write(const char *name, int64_t value) {
  writeName(name);
  if (yajl_gen_status_ok != yajl_gen_integer(generator, value)) {
    std::ostringstream err;
    err << "Can't write integer '" << name << "': '" << value << "' in json";
    throw std::runtime_error(err.str());
  }
}

void JsonWriter::write(const char *name, bool value) {
  writeName(name);
  if (yajl_gen_status_ok != yajl_gen_bool(generator, value)) {
    std::ostringstream err;
    err << "Can't write boolean '" << name << "': '" << value << "' in json";
    throw std::runtime_error(err.str());
  }
}

void JsonWriter::startArray(const char *name) {
  writeName(name);
  if (yajl_gen_status_ok != yajl_gen_array_open(generator)) {
    std::ostringstream err;
    err << "Can't start array " << name << " in json";
    throw std::runtime_error(err.str());
  }
}

void JsonWriter::startArray() {
  if (yajl_gen_status_ok != yajl_gen_array_open(generator)) {
    std::ostringstream err;
    err << "Can't start array in json";
    throw std::runtime_error(err.str());
  }
}

void JsonWriter::endArray() {
  if (yajl_gen_status_ok != yajl_gen_array_close(generator)) {
    std::ostringstream err;
    err << "Can't end array in json";
    throw std::runtime_error(err.str());
  }
}

void JsonWriter::startObject(const char *name) {
  writeName(name);
  if (yajl_gen_status_ok != yajl_gen_map_open(generator)) {
    std::ostringstream err;
    err << "Can't start object " << name << " in json";
    throw std::runtime_error(err.str());
  }
}

void JsonWriter::startObject() {
  if (yajl_gen_status_ok != yajl_gen_map_open(generator)) {
    std::ostringstream err;
    err << "Can't start object in json";
    throw std::runtime_error(err.str());
  }
}

void JsonWriter::endObject() {
  if (yajl_gen_status_ok != yajl_gen_map_close(generator)) {
    std::ostringstream err;
    err << "Can't end object in json";
    throw std::runtime_error(err.str());
  }
}

void exportDeck(const QString &filename, const DeckInfo &deck_info,
                const std::vector<std::unique_ptr<Card>> &cards,
                const std::vector<Tag> &tags) {
  JsonWriter writer(filename.toStdString());

  writer.startObject();

  writer.startObject("deck_info");
  writer.write("name", deck_info.name.toStdString());
  writer.startArray("fields");
  for (const auto &field : deck_info.word_fields) {
    writer.startObject();
    writer.write("name", field.name.toStdString());
    writer.write("is_key", field.is_key);
    writer.endObject();
  }
  writer.endArray();
  writer.endObject();

  writer.startArray("tags");
  for (const auto &tag : tags) {
    writer.startObject();
    writer.write("id", tag.id);
    writer.write("name", tag.name.toStdString());
    writer.endObject();
  }
  writer.endArray();

  writer.startArray("cards");
  WordCard *word_card;
  TableCard *table_card;
  FactCard *fact_card;
  for (const auto &card : cards) {
    writer.startObject();
    switch (card->getType()) {
      case Card::TypeWord:
        writer.write("type", "word");
        writer.startArray("fields");
        word_card = dynamic_cast<WordCard *>(card.get());
        for (size_t i = 0; i < word_card->fieldsNum(); ++i) {
          writer.write(word_card->getField(i).toStdString());
        }
        writer.endArray();
        break;
      case Card::TypeTable:
        writer.write("type", "table");
        table_card = dynamic_cast<TableCard *>(card.get());
        writer.write("name", table_card->getName().toStdString());
        writer.startArray("cells");
        for (size_t i = 0; i < table_card->rowsNum(); ++i) {
          writer.startArray();
          for (size_t j = 0; j < table_card->colsNum(); ++j) {
            writer.write(table_card->getCell(i, j).toStdString());
          }
          writer.endArray();
        }
        writer.endArray();
        writer.write("appendix", table_card->getAppendix().toStdString());
        break;
      case Card::TypeFact:
        writer.write("type", "fact");
        fact_card = dynamic_cast<FactCard *>(card.get());
        writer.write("name", fact_card->getName().toStdString());
        writer.write("description", fact_card->getDescription().toStdString());
        break;
    }
    writer.startArray("tags");
    for (const auto tag : card->getTags()) {
      writer.write(tag);
    }
    writer.endArray();

    writer.endObject();
  }
  writer.endArray();

  writer.endObject();
}
