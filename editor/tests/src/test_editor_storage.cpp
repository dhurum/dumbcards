/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include <gtest/gtest.h>
#include "editor_storage.h"
#include "fact_card.h"
#include "sqlite_wrapper.h"
#include "table_card.h"
#include "word_card.h"

TEST(TestStorageCreate, CreateDB) {
  DeckInfo deck_info{"test", {{"f1", true, 0}, {"f2", false, 0}}};
  auto storage = std::make_unique<EditorStorage>(
      "editor_storage_create_test.dc", deck_info);
  auto new_deck_info = storage->getDeckInfo();

  ASSERT_EQ(new_deck_info.name.toStdString(), "test");

  ASSERT_EQ(new_deck_info.word_fields.size(), 2);
  ASSERT_EQ(new_deck_info.word_fields[0].name, "f1");
  ASSERT_EQ(new_deck_info.word_fields[0].is_key, 1);
  ASSERT_NE(new_deck_info.word_fields[0].id, 0);
  ASSERT_EQ(new_deck_info.word_fields[1].name, "f2");
  ASSERT_EQ(new_deck_info.word_fields[1].is_key, 0);
  ASSERT_NE(new_deck_info.word_fields[1].id, 0);

  storage = std::make_unique<EditorStorage>("editor_storage_create_test.dc");
  new_deck_info = storage->getDeckInfo();

  ASSERT_EQ(new_deck_info.name.toStdString(), "test");

  ASSERT_EQ(new_deck_info.word_fields.size(), 2);
  ASSERT_EQ(new_deck_info.word_fields[0].name, "f1");
  ASSERT_EQ(new_deck_info.word_fields[0].is_key, 1);
  ASSERT_NE(new_deck_info.word_fields[0].id, 0);
  ASSERT_EQ(new_deck_info.word_fields[1].name, "f2");
  ASSERT_EQ(new_deck_info.word_fields[1].is_key, 0);
  ASSERT_NE(new_deck_info.word_fields[1].id, 0);
}

class TestStorage : public testing::Test {
 public:
  TestStorage();

  Sqlite::DB db;
  std::unique_ptr<EditorStorage> storage;
};

TestStorage::TestStorage() : db("editor_storage_test.dc") {
  db.exec("delete from deck_info");
  db.exec("delete from word_field_info");
  db.exec("delete from tag");
  db.exec("insert into deck_info (name) values ('test')");
  db.exec("insert into word_field_info (name, is_key) values ('f1', 1)");
  db.exec("insert into word_field_info (name, is_key) values ('f2', 0)");

  storage = std::make_unique<EditorStorage>("editor_storage_test.dc");
}

TEST_F(TestStorage, setSameValues) {
  auto deck_info = storage->getDeckInfo();
  storage->setDeckInfo(deck_info);

  auto new_deck_info = storage->getDeckInfo();
  ASSERT_EQ(new_deck_info.name.toStdString(), "test");

  ASSERT_EQ(new_deck_info.word_fields.size(), 2);
  ASSERT_EQ(new_deck_info.word_fields[0].name, "f1");
  ASSERT_EQ(new_deck_info.word_fields[0].is_key, 1);
  ASSERT_EQ(new_deck_info.word_fields[1].name, "f2");
  ASSERT_EQ(new_deck_info.word_fields[1].is_key, 0);

  storage = std::make_unique<EditorStorage>("editor_storage_test.dc");

  new_deck_info = storage->getDeckInfo();
  ASSERT_EQ(new_deck_info.name.toStdString(), "test");

  ASSERT_EQ(new_deck_info.word_fields.size(), 2);
  ASSERT_EQ(new_deck_info.word_fields[0].name, "f1");
  ASSERT_EQ(new_deck_info.word_fields[0].is_key, 1);
  ASSERT_EQ(new_deck_info.word_fields[1].name, "f2");
  ASSERT_EQ(new_deck_info.word_fields[1].is_key, 0);
}

TEST_F(TestStorage, setDeckName) {
  auto deck_info = storage->getDeckInfo();
  deck_info.name = "new_name";
  storage->setDeckInfo(deck_info);

  ASSERT_EQ(storage->getDeckInfo().name.toStdString(), "new_name");

  storage = std::make_unique<EditorStorage>("editor_storage_test.dc");
  ASSERT_EQ(storage->getDeckInfo().name.toStdString(), "new_name");
}

TEST_F(TestStorage, updateFieldName) {
  auto deck_info = storage->getDeckInfo();
  deck_info.word_fields[0].name = "new_name";
  storage->setDeckInfo(deck_info);

  auto new_deck_info = storage->getDeckInfo();
  ASSERT_EQ(new_deck_info.word_fields.size(), 2);
  ASSERT_EQ(new_deck_info.word_fields[0].name, "new_name");
  ASSERT_EQ(new_deck_info.word_fields[0].is_key, 1);
  ASSERT_EQ(new_deck_info.word_fields[1].name, "f2");
  ASSERT_EQ(new_deck_info.word_fields[1].is_key, 0);

  storage = std::make_unique<EditorStorage>("editor_storage_test.dc");

  new_deck_info = storage->getDeckInfo();
  ASSERT_EQ(new_deck_info.word_fields.size(), 2);
  ASSERT_EQ(new_deck_info.word_fields[0].name, "new_name");
  ASSERT_EQ(new_deck_info.word_fields[0].is_key, 1);
  ASSERT_EQ(new_deck_info.word_fields[1].name, "f2");
  ASSERT_EQ(new_deck_info.word_fields[1].is_key, 0);
}

TEST_F(TestStorage, addField) {
  auto deck_info = storage->getDeckInfo();
  deck_info.word_fields.push_back({"f3", 1, 0});
  storage->setDeckInfo(deck_info);

  auto new_deck_info = storage->getDeckInfo();
  ASSERT_EQ(new_deck_info.word_fields.size(), 3);
  ASSERT_EQ(new_deck_info.word_fields[0].name, "f1");
  ASSERT_EQ(new_deck_info.word_fields[0].is_key, 1);
  ASSERT_EQ(new_deck_info.word_fields[1].name, "f2");
  ASSERT_EQ(new_deck_info.word_fields[1].is_key, 0);
  ASSERT_EQ(new_deck_info.word_fields[2].name, "f3");
  ASSERT_EQ(new_deck_info.word_fields[2].is_key, 0);
  ASSERT_NE(new_deck_info.word_fields[2].id, 0);

  storage = std::make_unique<EditorStorage>("editor_storage_test.dc");

  new_deck_info = storage->getDeckInfo();
  ASSERT_EQ(new_deck_info.word_fields.size(), 3);
  ASSERT_EQ(new_deck_info.word_fields[0].name, "f1");
  ASSERT_EQ(new_deck_info.word_fields[0].is_key, 1);
  ASSERT_EQ(new_deck_info.word_fields[1].name, "f2");
  ASSERT_EQ(new_deck_info.word_fields[1].is_key, 0);
  ASSERT_EQ(new_deck_info.word_fields[2].name, "f3");
  ASSERT_EQ(new_deck_info.word_fields[2].is_key, 0);
  ASSERT_NE(new_deck_info.word_fields[2].id, 0);
}

TEST_F(TestStorage, addTag) {
  Tag tag{0, "t1"};
  storage->addTag(tag);

  ASSERT_NE(tag.id, 0);

  auto tags = storage->getTags();
  ASSERT_EQ(tags.size(), 1);
  ASSERT_EQ(tags[0].name, "t1");
  ASSERT_NE(tags[0].id, 0);
}

TEST_F(TestStorage, updateTag) {
  Tag tag{0, "t1"};
  storage->addTag(tag);

  tag.name = "t2";
  storage->updateTag(tag);

  auto tags = storage->getTags();
  ASSERT_EQ(tags.size(), 1);
  ASSERT_EQ(tags[0].name, "t2");
  ASSERT_NE(tags[0].id, 0);
}

TEST_F(TestStorage, deleteTag) {
  Tag tag{0, "t1"};
  storage->addTag(tag);

  storage->deleteTag(tag);

  auto tags = storage->getTags();
  ASSERT_EQ(tags.size(), 0);
}

class TestStorageCards : public testing::Test {
 public:
  TestStorageCards();

  Sqlite::DB db;
  std::unique_ptr<EditorStorage> storage;
};

TestStorageCards::TestStorageCards() : db("editor_storage_test.dc") {
  db.exec("delete from deck_info");
  db.exec("delete from word_field_info");
  db.exec("delete from card_data");
  db.exec("delete from card_instance");
  db.exec("delete from card_tag");
  db.exec("delete from tag");
  db.exec("insert into deck_info (name) values ('test')");
  db.exec("insert into word_field_info (name, is_key) values ('f1', 1)");
  db.exec("insert into word_field_info (name, is_key) values ('f2', 1)");
  db.exec("insert into word_field_info (name, is_key) values ('f3', 0)");

  storage = std::make_unique<EditorStorage>("editor_storage_test.dc");
  Tag tags[] = {{0, "t1"}, {0, "t2"}};
  storage->addTag(tags[0]);
  storage->addTag(tags[1]);
}

TEST_F(TestStorageCards, addWordCard) {
  auto tags = storage->getTags();
  WordCard card(0, std::vector<QString>{"a", "1"}, {tags[0].id});
  storage->addCard(card);

  ASSERT_NE(card.getId(), 0);

  auto instances_num =
      db.prepare("select count(*) from card_instance where data_id = :id");
  instances_num->bind(":id", card.getId());
  instances_num->exec();
  ASSERT_EQ(instances_num->get<int>(0), 2);

  auto cards = storage->allCards();
  ASSERT_EQ(cards.size(), 1);
  ASSERT_EQ(cards[0]->getType(), Card::TypeWord);

  auto new_card = static_cast<WordCard*>(cards[0].get());
  ASSERT_EQ(new_card->getField(0), "a");
  ASSERT_EQ(new_card->getField(1), "1");

  auto card_tags = new_card->getTags();
  ASSERT_EQ(card_tags.size(), 1);
  ASSERT_EQ(card_tags[0], tags[0].id);
}

TEST_F(TestStorageCards, addTableCard) {
  auto tags = storage->getTags();
  TableCard card(0, "table",
                 std::vector<std::vector<QString>>{{"b", "2"}, {"c", "3"}}, "d",
                 {tags[0].id, tags[1].id});
  storage->addCard(card);

  ASSERT_NE(card.getId(), 0);

  auto instances_num =
      db.prepare("select count(*) from card_instance where data_id = :id");
  instances_num->bind(":id", card.getId());
  instances_num->exec();
  ASSERT_EQ(instances_num->get<int>(0), 1);

  auto cards = storage->allCards();
  ASSERT_EQ(cards.size(), 1);
  ASSERT_EQ(cards[0]->getType(), Card::TypeTable);

  auto new_card = static_cast<TableCard*>(cards[0].get());
  ASSERT_EQ(new_card->getName(), "table");
  ASSERT_EQ(new_card->getCell(0, 0), "b");
  ASSERT_EQ(new_card->getCell(0, 1), "2");
  ASSERT_EQ(new_card->getCell(1, 0), "c");
  ASSERT_EQ(new_card->getCell(1, 1), "3");
  ASSERT_EQ(new_card->getAppendix(), "d");

  auto card_tags = new_card->getTags();
  ASSERT_EQ(card_tags.size(), 2);
  ASSERT_EQ(card_tags[0], tags[0].id);
  ASSERT_EQ(card_tags[1], tags[1].id);
}

TEST_F(TestStorageCards, addFactCard) {
  FactCard card(0, "fact", "e", {});
  storage->addCard(card);

  ASSERT_NE(card.getId(), 0);

  auto instances_num =
      db.prepare("select count(*) from card_instance where data_id = :id");
  instances_num->bind(":id", card.getId());
  instances_num->exec();
  ASSERT_EQ(instances_num->get<int>(0), 1);

  auto cards = storage->allCards();
  ASSERT_EQ(cards.size(), 1);
  ASSERT_EQ(cards[0]->getType(), Card::TypeFact);

  auto new_card = static_cast<FactCard*>(cards[0].get());
  ASSERT_EQ(new_card->getName(), "fact");
  ASSERT_EQ(new_card->getDescription(), "e");

  auto card_tags = new_card->getTags();
  ASSERT_EQ(card_tags.size(), 0);
}

TEST_F(TestStorageCards, updateCard) {
  auto tags = storage->getTags();
  FactCard card(0, "fact", "fact", {tags[0].id});
  storage->addCard(card);

  FactCard upd_card(card.getId(), "fact2", "f", {tags[1].id});
  storage->updateCard(upd_card);

  auto cards = storage->allCards();
  ASSERT_EQ(cards.size(), 1);
  ASSERT_EQ(cards[0]->getType(), Card::TypeFact);

  auto new_card = static_cast<FactCard*>(cards[0].get());
  ASSERT_EQ(new_card->getName(), "fact2");
  ASSERT_EQ(new_card->getDescription(), "f");

  auto card_tags = new_card->getTags();
  ASSERT_EQ(card_tags.size(), 1);
  ASSERT_EQ(card_tags[0], tags[1].id);
}

TEST_F(TestStorageCards, deleteCard) {
  auto tags = storage->getTags();
  FactCard card(0, "fact", "fact", {tags[0].id});
  storage->addCard(card);

  storage->deleteCard(card);

  auto cards = storage->allCards();
  ASSERT_EQ(cards.size(), 0);

  auto instances_num =
      db.prepare("select count(*) from card_instance where data_id = :id");
  instances_num->bind(":id", card.getId());
  instances_num->exec();
  ASSERT_EQ(instances_num->get<int>(0), 0);

  auto tags_num =
      db.prepare("select count(*) from card_tag where data_id = :id");
  tags_num->bind(":id", card.getId());
  tags_num->exec();
  ASSERT_EQ(tags_num->get<int>(0), 0);
}
