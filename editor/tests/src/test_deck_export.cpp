/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include <gtest/gtest.h>
#include <fstream>
#include <streambuf>
#include "deck_export.h"
#include "editor_storage.h"
#include "fact_card.h"
#include "table_card.h"
#include "word_card.h"

TEST(DeckExport, export) {
  DeckInfo deck_info = {"test", {{"f 1", true, 0}, {"f 2", false, 0}}};
  EditorStorage storage("editor_export_test.dc", deck_info);
  Tag tags[] = {{0, "tag 1"}, {0, "tag 2"}};
  storage.addTag(tags[0]);
  storage.addTag(tags[1]);

  WordCard word_cards[] = {
      {0, std::vector<QString>{"a", "1"}, {}},
      {0, std::vector<QString>{"b", "2"}, {tags[0].id, tags[1].id}}};
  TableCard table_card(0, "table", {{"1", "2"}, {"3", "4"}}, "app", {});
  FactCard fact_card(0, "fact", "desc", {tags[0].id});

  storage.addCard(word_cards[0]);
  storage.addCard(word_cards[1]);
  storage.addCard(fact_card);
  storage.addCard(table_card);

  exportDeck("editor_export_test.json", storage.getDeckInfo(),
             storage.allCards(), storage.getTags());

  std::ifstream json_stream("editor_export_test.json");
  std::string json_str((std::istreambuf_iterator<char>(json_stream)),
                       std::istreambuf_iterator<char>());
  std::string json_str_goal(
      R"({
    "deck_info": {
        "name": "test",
        "fields": [
            {
                "name": "f 1",
                "is_key": true
            },
            {
                "name": "f 2",
                "is_key": false
            }
        ]
    },
    "tags": [
        {
            "id": 1,
            "name": "tag 1"
        },
        {
            "id": 2,
            "name": "tag 2"
        }
    ],
    "cards": [
        {
            "type": "word",
            "fields": [
                "a",
                "1"
            ],
            "tags": [

            ]
        },
        {
            "type": "word",
            "fields": [
                "b",
                "2"
            ],
            "tags": [
                1,
                2
            ]
        },
        {
            "type": "fact",
            "name": "fact",
            "description": "desc",
            "tags": [
                1
            ]
        },
        {
            "type": "table",
            "name": "table",
            "cells": [
                [
                    "1",
                    "2"
                ],
                [
                    "3",
                    "4"
                ]
            ],
            "appendix": "app",
            "tags": [

            ]
        }
    ]
}
)");

  ASSERT_EQ(json_str, json_str_goal);
}
