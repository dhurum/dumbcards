## Simple flashcards program

Dumbcards is a simple, plain dumb flashcards program, written using Qt5.  
It does not have any fancy repetition algorithms.  
After being shown 5 times, card is considered correct and is not shown anymore, unless specified in filter settings.  
You can create tags and assign them to cards.  
In client you can set filtering by tags, last answer time, and correctness.

### Card types:
#### Word
Has a list of fields (set at deck creation), at least on of them is key.  
In client key field is shown, and then rest of fields.  
For each key field separate card instance is created internally.

#### Table
Can have arbitrary number of rows and columns and a text appendix.  
In client title is shown, and then table with appendix.

#### Fact
Simple text.  
In client title is shown, and then text.
