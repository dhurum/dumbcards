#!/bin/bash

setup=
teardown=

while [ ${#@} -gt 0 ]; do
  if [ $1 == "--setup" ]; then
    setup=$2
  elif [ $1 == "--teardown" ]; then
    teardown=$2
  else
    break
  fi
  shift 2
done

if [ ${#setup} -gt 0 ]; then
  $setup
fi

$@
ret=$?

if [ ${#teardown} -gt 0 ]; then
  $teardown
fi

exit $ret
