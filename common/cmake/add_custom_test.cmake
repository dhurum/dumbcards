include(CMakeParseArguments)

function(ADD_CUSTOM_TEST NAME)
  if(NOT NAME)
    message(SEND_ERROR "no NAME provided to ADD_CUSTOM_TEST")
  endif()
  
  set(one_value_args SETUP TEARDOWN)
  cmake_parse_arguments(ARGS "" "${one_value_args}" "" ${ARGN})

  if(NOT ARGS_UNPARSED_ARGUMENTS)
    message(SEND_ERROR "no command provided to ADD_CUSTOM_TEST")
  endif()

  set(command ${CMAKE_SOURCE_DIR}/common/scripts/run_test.sh)
  if(ARGS_SETUP)
    list(APPEND command "--setup" ${ARGS_SETUP})
  endif()
  if(ARGS_TEARDOWN)
    list(APPEND command "--teardown" ${ARGS_TEARDOWN})
  endif()
  list(APPEND command ${ARGS_UNPARSED_ARGUMENTS})

  add_test(${NAME} ${command})
endfunction()
