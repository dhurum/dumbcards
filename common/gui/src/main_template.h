/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <stdio.h>
#include <QCommandLineParser>
#include <QLibraryInfo>
#include <QMessageBox>
#include <QTranslator>
#include <memory>
#include "application.h"
#include "card.pb.h"
#include "main_window.h"

template <const char *description> int main_template(int argc, char *argv[]) {
  GOOGLE_PROTOBUF_VERIFY_VERSION;
  Application app(argc, argv);
  QCoreApplication::setApplicationVersion("0.1");

  QTranslator qt_translator;
  qt_translator.load("qt_" + QLocale::system().name(),
                     QLibraryInfo::location(QLibraryInfo::TranslationsPath));
  app.installTranslator(&qt_translator);

  QTranslator app_translator;
  app_translator.load(QLocale(), "dumbcards", "_",
                      INSTALL_PREFIX "/share/dumbcards/translations/", ".qm");
  app.installTranslator(&app_translator);

  QCommandLineParser parser;
  parser.setApplicationDescription(description);
  parser.addHelpOption();
  parser.addVersionOption();
  parser.addPositionalArgument("deck", QObject::tr("Deck file"));

  parser.process(app);

  QString deck_file;
  auto args = parser.positionalArguments();
  if (!args.isEmpty()) {
    deck_file = args[0];
  }

  int ret;
  try {
    auto window = std::make_unique<MainWindow>(deck_file);
    window->show();
    ret = app.exec();
  } catch (std::exception &e) {
    fprintf(stderr, "%s\n", e.what());
    ret = 1;
    QMessageBox(QMessageBox::Critical, QObject::tr("Critical Error"), e.what(),
                QMessageBox::Ok)
        .exec();
  }

  google::protobuf::ShutdownProtobufLibrary();
  return ret;
}
