/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <QLayout>

class HorizontalWrappingLayout : public QLayout {
  Q_OBJECT

 public:
  ~HorizontalWrappingLayout();
  void addItem(QLayoutItem *item) override;
  int count() const override;
  QLayoutItem *itemAt(int index) const override;
  QLayoutItem *takeAt(int index) override;
  Qt::Orientations expandingDirections() const override;
  bool hasHeightForWidth() const override;
  int heightForWidth(int width) const override;
  void setGeometry(const QRect &rect) override;
  QSize sizeHint() const override;
  QSize minimumSize() const override;

 private:
  QList<QLayoutItem *> items;
  mutable bool cached_size_valid = false;
  mutable int cached_width = 0;
  mutable int cached_height = 0;

  int setWidth(int width, bool test = false) const;
};
