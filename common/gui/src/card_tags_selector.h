/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <QLabel>
#include <QWidget>
#include <list>
#include <vector>
#include "tag.h"

class HorizontalWrappingLayout;

class TagWidget : public QLabel {
  Q_OBJECT

 public:
  TagWidget(const QString &name);
};

class CardTagsSelector : public QWidget {
  Q_OBJECT

 public:
  CardTagsSelector(const std::vector<Tag> &tags,
                   const std::vector<int64_t> *card_tags = nullptr);
  std::vector<int64_t> get();

 private:
  std::vector<Tag> tags;
  struct TagInfo {
    int64_t id;
    TagWidget *widget;
  };
  std::list<TagInfo> selected_tags;
  HorizontalWrappingLayout *tags_layout;

  void deleteTag(std::list<TagInfo>::const_iterator it);

 private slots:
  void addTag(int index);
};
