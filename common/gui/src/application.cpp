/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "application.h"
#include <stdio.h>
#include <QMessageBox>
#include <exception>

Application::Application(int &argc, char *argv[]) : QApplication(argc, argv) {}

bool Application::notify(QObject *receiver, QEvent *e) {
  try {
    return QApplication::notify(receiver, e);
  } catch (std::exception &e) {
    fprintf(stderr, "%s\n", e.what());
    QMessageBox(QMessageBox::Critical, tr("Critical Error"), e.what(),
                QMessageBox::Ok)
        .exec();
  }
  exit(1);
  return false;
}
