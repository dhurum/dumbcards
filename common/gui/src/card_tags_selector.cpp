/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "card_tags_selector.h"
#include <QComboBox>
#include <QVBoxLayout>
#include "horizontal_wrapping_layout.h"
#include "qt_helpers.h"

TagWidget::TagWidget(const QString &name) {
  setTextFormat(Qt::RichText);
  setText(name + " <a href=\"/\">x</a>");
}

CardTagsSelector::CardTagsSelector(const std::vector<Tag> &tags,
                                   const std::vector<int64_t> *card_tags)
    : tags(tags) {
  auto layout = new QVBoxLayout();
  setLayout(layout);

  tags_layout = new HorizontalWrappingLayout();
  layout->addLayout(tags_layout);

  auto tag_picker = new QComboBox();
  tag_picker->addItem("");

  std::vector<int64_t>::const_iterator it;
  if (card_tags) {
    it = card_tags->begin();
  }
  for (size_t i = 0; i < tags.size(); ++i) {
    tag_picker->addItem(tags[i].name);
    if (card_tags && (it != card_tags->end()) && (*it == tags[i].id)) {
      addTag(i + 1);
      ++it;
    }
  }
  CONNECT_OVERLOAD(tag_picker, currentIndexChanged, this, addTag, void, (int));
  layout->addWidget(tag_picker);
}

void CardTagsSelector::addTag(int index) {
  if (index <= 0) {
    return;
  }
  --index;
  auto id = tags[index].id;
  auto it = selected_tags.begin();
  for (; it != selected_tags.end(); ++it) {
    if (id == it->id) {
      return;
    }
    if (id < it->id) {
      break;
    }
  }
  it = selected_tags.insert(it, {id, new TagWidget(tags[index].name)});
  CONNECT_FUNCTION(it->widget, linkActivated,
                   ([this, it](const QString &) { deleteTag(it); }));

  tags_layout->addWidget(it->widget);
}

void CardTagsSelector::deleteTag(std::list<TagInfo>::const_iterator it) {
  tags_layout->removeWidget(it->widget);
  delete it->widget;
  selected_tags.erase(it);
}

std::vector<int64_t> CardTagsSelector::get() {
  std::vector<int64_t> selected_ids;

  for (const auto &tag : selected_tags) {
    selected_ids.push_back(tag.id);
  }
  return selected_ids;
}
