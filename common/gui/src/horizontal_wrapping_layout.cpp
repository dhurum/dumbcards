/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "horizontal_wrapping_layout.h"
#include <QApplication>
#include <QStyle>

HorizontalWrappingLayout::~HorizontalWrappingLayout() {
  QLayoutItem *item;
  while ((item = takeAt(0))) {
    delete item;
  }
}

void HorizontalWrappingLayout::addItem(QLayoutItem *item) {
  items.append(item);
  cached_size_valid = false;
}

int HorizontalWrappingLayout::count() const {
  return items.size();
}

QLayoutItem *HorizontalWrappingLayout::itemAt(int index) const {
  return items.value(index);
}

QLayoutItem *HorizontalWrappingLayout::takeAt(int index) {
  if ((index < 0) || (index >= items.size())) {
    return nullptr;
  }
  cached_size_valid = false;
  return items.takeAt(index);
}

Qt::Orientations HorizontalWrappingLayout::expandingDirections() const {
  return 0;
}

bool HorizontalWrappingLayout::hasHeightForWidth() const {
  return true;
}

int HorizontalWrappingLayout::heightForWidth(int width) const {
  return setWidth(width, true);
}

void HorizontalWrappingLayout::setGeometry(const QRect &rect) {
  cached_width = rect.width();
  cached_height = setWidth(cached_width);
  cached_size_valid = true;
  QLayout::setGeometry(rect);
}

QSize HorizontalWrappingLayout::sizeHint() const {
  return minimumSize();
}

QSize HorizontalWrappingLayout::minimumSize() const {
  if (cached_size_valid) {
    return QSize(cached_width, cached_height);
  }
  int width = 0;
  for (const auto &item : items) {
    width = std::max(width, item->minimumSize().width());
  }
  width = std::max(width, cached_width);

  return QSize(width, heightForWidth(width));
}

int HorizontalWrappingLayout::setWidth(int width, bool test) const {
  const auto style = qApp->style();
  const int horizontal_spacing = 10;
  const int vertical_spacing = style->layoutSpacing(
      QSizePolicy::DefaultType, QSizePolicy::DefaultType, Qt::Vertical);
  const auto margins = contentsMargins();
  int x = margins.left();
  int y = margins.top();
  const int max_x = width - margins.right();
  int line_height = 0;

  for (auto &item : items) {
    const auto item_width = item->sizeHint().width();
    if (((x + item_width) >= max_x) && line_height) {
      x = margins.left();
      y += line_height + vertical_spacing;
      line_height = 0;
    }
    if (!test) {
      item->setGeometry(QRect(QPoint(x, y), item->sizeHint()));
    }
    x += item_width + horizontal_spacing;
    line_height = std::max(line_height, item->sizeHint().height());
  }
  return y + line_height + margins.bottom();
}
