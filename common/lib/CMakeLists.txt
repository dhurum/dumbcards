set(TARGET common_lib)

set(COMMON_LIB_INCLUDES
  ${CMAKE_CURRENT_SOURCE_DIR}/src 
  ${CMAKE_CURRENT_BINARY_DIR} 
  ${Qt5Core_INCLUDES}
  ${PROTOBUF_INCLUDE_DIRS}
  CACHE INTERNAL ""
)
set (COMMON_LIB_DEFINITIONS
  ${Qt5Core_DEFINITIONS}
  CACHE INTERNAL ""
)

include_directories(${COMMON_LIB_INCLUDES})
add_definitions(${COMMON_LIB_DEFINITIONS})

protobuf_generate_cpp(PROTO_SRCS PROTO_HDRS src/card.proto)

add_library(${TARGET}
  src/card.cpp
  src/word_card.cpp
  src/table_card.cpp
  src/fact_card.cpp
  src/storage.cpp
  src/qt_helpers.cpp
  src/sqlite_wrapper.cpp
  ${PROTO_SRCS}
)

target_link_libraries(${TARGET}
  ${SQLITE_LIB}
  ${Qt5Core_LIBRARIES} 
  ${PROTOBUF_LIBRARIES}
)

if (CMAKE_BUILD_TYPE STREQUAL "Debug")
  add_subdirectory(tests)
endif()
