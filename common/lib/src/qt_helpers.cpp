/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "qt_helpers.h"
#include <QString>

bool matchString(const QString &string, const QString &match, bool match_case,
                 bool whole_text) {
  if (whole_text) {
    return !string.compare(
        match, match_case ? Qt::CaseSensitive : Qt::CaseInsensitive);
  }
  return string.contains(match,
                         match_case ? Qt::CaseSensitive : Qt::CaseInsensitive);
}
