/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "word_card.h"
#include <QObject>
#include <QString>
#include "qt_helpers.h"

WordCard::WordCard(int64_t id, const std::string &bytes,
                   const std::vector<int64_t> &tags, size_t key_field_id,
                   int correct)
    : Card(id, correct, tags), key_field_id(key_field_id) {
  if (!data.ParseFromString(bytes)) {
    throw std::runtime_error(QObject::tr("Can't parse card").toStdString());
  }
}

WordCard::WordCard(int64_t id, const std::vector<QString> &fields,
                   const std::vector<int64_t> &tags)
    : Card(id, 0, tags) {
  for (auto &field : fields) {
    data.add_field(field.toStdString());
  }
}

const QString WordCard::getName() const {
  return data.field(0).c_str();
}

size_t WordCard::fieldsNum() const {
  return data.field_size();
}

const QString WordCard::getField(size_t index) const {
  return data.field(index).c_str();
}

size_t WordCard::keyFieldId() const {
  return key_field_id;
}

std::string WordCard::serialize() const {
  std::string serialized;

  if (!data.SerializeToString(&serialized)) {
    throw std::runtime_error(QObject::tr("Can't serialize card").toStdString());
  }
  return serialized;
}

int WordCard::getType() const {
  return Card::TypeWord;
}

bool WordCard::matchAll(const QString &match, bool match_case,
                        bool whole_text) const {
  for (size_t i = 0; i < fieldsNum(); ++i) {
    if (matchString(getField(i), match, match_case, whole_text)) {
      return true;
    }
  }
  return false;
}
