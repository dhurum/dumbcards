/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include "card.h"
#include "card.pb.h"

class QString;

class FactCard : public Card {
 public:
  FactCard(int64_t id, const std::string &bytes,
           const std::vector<int64_t> &tags, int correct = 0);
  FactCard(int64_t id, const QString &title, const QString &description,
           const std::vector<int64_t> &tags);
  const QString getName() const override;
  const QString getDescription() const;
  std::string serialize() const override;
  int getType() const override;
  bool matchAll(const QString &match, bool match_case,
                bool whole_text) const override;

 private:
  Fact data;
};
