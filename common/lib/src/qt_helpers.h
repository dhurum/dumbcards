/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

class QString;

#define GET_CLASS_FROM_PTR(ptr) std::remove_pointer_t<decltype(ptr)>

#define CONNECT(sender, signal, receiver, slot)                  \
  connect(sender, &GET_CLASS_FROM_PTR(sender)::signal, receiver, \
          &GET_CLASS_FROM_PTR(receiver)::slot);

#define CONNECT_FUNCTION(sender, signal, function) \
  connect(sender, &GET_CLASS_FROM_PTR(sender)::signal, function);

#define CONNECT_OVERLOAD(sender, signal, receiver, slot, retval, args)         \
  connect(sender, static_cast<retval(GET_CLASS_FROM_PTR(sender)::*) args>(     \
                      &GET_CLASS_FROM_PTR(sender)::signal),                    \
          receiver, static_cast<retval(GET_CLASS_FROM_PTR(receiver)::*) args>( \
                        &GET_CLASS_FROM_PTR(receiver)::slot));

bool matchString(const QString &string, const QString &match, bool match_case,
                 bool whole_text);
