/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <string>
#include <vector>

class QString;

class Card {
 public:
  enum { TypeWord = 0, TypeTable, TypeFact };
  Card(int64_t id, int correct, const std::vector<int64_t> &tags);
  virtual ~Card() = default;
  int64_t getId() const;
  virtual const QString getName() const = 0;
  int getCorrect() const;
  void resetCorrect();
  virtual std::string serialize() const = 0;
  virtual int getType() const = 0;
  const std::vector<int64_t> &getTags() const;
  bool match(bool match_all, const QString &match_str, bool match_case,
             bool whole_text, const std::vector<int64_t> &match_tags,
             bool all_tags) const;

  friend void setCardId(Card &card, int64_t id);

 private:
  int64_t id = 0;  // In editor card_data.rowid, in client card_instance.rowid
  int correct = 0;
  std::vector<int64_t> tags;

  bool matchTags(const std::vector<int64_t> &match_tags, bool all_tags) const;
  virtual bool matchAll(const QString &match_str, bool match_case,
                        bool whole_text) const = 0;
};

void setCardId(Card &card, int64_t id);
