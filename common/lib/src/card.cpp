/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "card.h"
#include <QString>
#include <stdexcept>
#include "qt_helpers.h"

Card::Card(int64_t id, int correct, const std::vector<int64_t> &tags)
    : id(id), correct(correct), tags(tags) {}

int64_t Card::getId() const {
  return id;
}

int Card::getCorrect() const {
  return correct;
}

void Card::resetCorrect() {
  correct = 0;
}

const std::vector<int64_t> &Card::getTags() const {
  return tags;
}

bool Card::matchTags(const std::vector<int64_t> &match_tags,
                     bool all_tags) const {
  if (match_tags.empty()) {
    return true;
  }

  size_t found = 0;
  std::vector<int64_t>::const_iterator it = match_tags.begin();

  for (const auto &tag : tags) {
    if (it == match_tags.end()) {
      break;
    }
    if (*it == tag) {
      if (!all_tags) {
        return true;
      }
      ++found;
      ++it;
    }
  }
  if (found == match_tags.size()) {
    return true;
  }
  return false;
}

bool Card::match(bool match_all, const QString &match_str, bool match_case,
                 bool whole_text, const std::vector<int64_t> &match_tags,
                 bool all_tags) const {
  bool matches = false;

  if (match_all) {
    matches = matchAll(match_str, match_case, whole_text);
  } else {
    matches = matchString(getName(), match_str, match_case, whole_text);
  }
  return matches && matchTags(match_tags, all_tags);
}

void setCardId(Card &card, int64_t id) {
  card.id = id;
}
