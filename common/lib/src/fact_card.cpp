/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "fact_card.h"
#include <QObject>
#include <QString>
#include "qt_helpers.h"

FactCard::FactCard(int64_t id, const std::string &bytes,
                   const std::vector<int64_t> &tags, int correct)
    : Card(id, correct, tags) {
  if (!data.ParseFromString(bytes)) {
    throw std::runtime_error(QObject::tr("Can't parse card").toStdString());
  }
}
FactCard::FactCard(int64_t id, const QString &title, const QString &description,
                   const std::vector<int64_t> &tags)
    : Card(id, 0, tags) {
  data.set_title(title.toStdString());
  data.set_description(description.toStdString());
}

const QString FactCard::getName() const {
  return data.title().c_str();
}

const QString FactCard::getDescription() const {
  return data.description().c_str();
}

std::string FactCard::serialize() const {
  std::string serialized;

  if (!data.SerializeToString(&serialized)) {
    throw std::runtime_error(QObject::tr("Can't serialize card").toStdString());
  }
  return serialized;
}

int FactCard::getType() const {
  return Card::TypeFact;
}

bool FactCard::matchAll(const QString &match, bool match_case,
                        bool whole_text) const {
  if (matchString(getName(), match, match_case, whole_text)
      || matchString(getDescription(), match, match_case, whole_text)) {
    return true;
  }
  return false;
}
