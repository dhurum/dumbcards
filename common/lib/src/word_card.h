/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <string>
#include <vector>
#include "card.h"
#include "card.pb.h"

class QString;

class WordCard : public Card {
 public:
  WordCard(int64_t id, const std::string &bytes,
           const std::vector<int64_t> &tags, size_t key_field_id = 0,
           int correct = 0);
  WordCard(int64_t id, const std::vector<QString> &fields,
           const std::vector<int64_t> &tags);
  const QString getName() const override;
  size_t fieldsNum() const;
  const QString getField(size_t index) const;
  size_t keyFieldId() const;
  std::string serialize() const override;
  int getType() const override;
  bool matchAll(const QString &match, bool match_case,
                bool whole_text) const override;

 private:
  Word data;
  size_t key_field_id = 0;
};
