#include "sqlite_wrapper.h"
#include <sqlite3.h>
#include <sstream>
#include <stdexcept>
#include <string>

using namespace Sqlite;

DB::DB(const std::string &name) {
  if (SQLITE_OK != sqlite3_open(name.c_str(), &db)) {
    std::ostringstream err;
    err << "Can't connect to database " << name;
    error(err.str());
  }
}

DB::~DB() {
  sqlite3_close(db);
}

void DB::error(const std::string &msg) const {
  std::ostringstream err;
  err << msg << ": " << sqlite3_errmsg(db);
  throw std::runtime_error(err.str());
}

std::unique_ptr<Stmt> DB::prepare(const std::string &query) {
  return std::make_unique<Stmt>(query, db);
}

std::unique_ptr<Stmt> DB::exec(const std::string &query) {
  auto stmt = prepare(query);
  stmt->exec();
  return stmt;
}

void DB::transaction() {
  exec("begin immediate");
}

void DB::commit() {
  exec("commit");
}

void DB::rollback() {
  exec("rollback");
}

int64_t DB::lastInsertRowid() {
  return sqlite3_last_insert_rowid(db);
}

Stmt::Stmt(const std::string &query, sqlite3 *db) : query(query) {
  if (SQLITE_OK != sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr)) {
    std::ostringstream err;
    err << "Can't prepare statement '" << query << "': " << sqlite3_errmsg(db);
    throw std::runtime_error(err.str());
  }
}

Stmt::~Stmt() {
  if (stmt) {
    sqlite3_finalize(stmt);
  }
}

void Stmt::error(const std::string &msg) const {
  std::ostringstream err;
  err << msg << ": " << sqlite3_errmsg(sqlite3_db_handle(stmt));
  throw std::runtime_error(err.str());
}

int Stmt::paramIndex(const std::string &param) {
  auto it = param_indexes.find(param);
  if (it != param_indexes.end()) {
    return it->second;
  }
  int index = sqlite3_bind_parameter_index(stmt, param.c_str());
  if (!index) {
    std::ostringstream err;
    err << "Can't find parameter " << param << " in statement '" << query
        << "' to bind";
    throw std::runtime_error(err.str());
  }
  param_indexes[param] = index;
  return index;
}

void Stmt::checkBind(const std::string &param, int ret) {
  if (SQLITE_OK != ret) {
    std::ostringstream err;
    err << "Can't bind parameter " << param << " in statement '" << query
        << "'";
    error(err.str());
  }
}

void Stmt::bind(const std::string &param, int value) {
  tryReset();
  checkBind(param, sqlite3_bind_int(stmt, paramIndex(param), value));
}

void Stmt::bind(const std::string &param, int64_t value) {
  tryReset();
  checkBind(param, sqlite3_bind_int64(stmt, paramIndex(param), value));
}

void Stmt::bind(const std::string &param, const std::string &value) {
  tryReset();
  checkBind(param, sqlite3_bind_text(stmt, paramIndex(param), value.c_str(),
                                     value.length(), nullptr));
}

void Stmt::bind(const std::string &param, const void *value, size_t len) {
  tryReset();
  checkBind(param,
            sqlite3_bind_blob(stmt, paramIndex(param), value, len, nullptr));
}

bool Stmt::step() {
  is_active = false;
  int ret = sqlite3_step(stmt);
  if (SQLITE_DONE == ret) {
    return false;
  }
  if (SQLITE_ROW != ret) {
    std::ostringstream err;
    err << "Can't execute statement '" << query << "'";
    error(err.str());
  }
  is_active = true;
  return true;
}

void Stmt::reset() {
  if (SQLITE_OK != sqlite3_reset(stmt)) {
    std::ostringstream err;
    err << "Can't reset statement '" << query << "'";
    error(err.str());
  }
  reseted = true;
}

void Stmt::tryReset() {
  if (!reseted) {
    reset();
  }
}

void Stmt::exec() {
  reset();
  reseted = false;
  if (!step()) {
    pending_data = false;
    return;
  }
  pending_data = true;
}

bool Stmt::next() {
  if (!is_active) {
    return false;
  }
  if (pending_data) {
    pending_data = false;
    return true;
  }
  return step();
}

bool Stmt::active() {
  return is_active;
}

void Stmt::checkGet(int index) {
  if (!is_active) {
    std::ostringstream err;
    err << "Can't get any results from statement '" << query
        << "', it is not active";
    throw std::runtime_error(err.str());
  }
  int count = sqlite3_column_count(stmt);
  if (index >= count) {
    std::ostringstream err;
    err << "Can't get column " << index << " from statement '" << query
        << "', it returns only " << count << " columns";
    throw std::runtime_error(err.str());
  }
  pending_data = false;
}

template <> int Stmt::get(int index) {
  checkGet(index);
  return sqlite3_column_int(stmt, index);
}

template <> int64_t Stmt::get(int index) {
  checkGet(index);
  return sqlite3_column_int64(stmt, index);
}

template <> std::string Stmt::get(int index) {
  checkGet(index);
  return std::string(
      reinterpret_cast<const char *>(sqlite3_column_text(stmt, index)));
}

template <> std::pair<const void *, size_t> Stmt::get(int index) {
  checkGet(index);
  return std::make_pair<const void *, size_t>(
      sqlite3_column_blob(stmt, index), sqlite3_column_bytes(stmt, index));
}
