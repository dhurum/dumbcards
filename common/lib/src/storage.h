/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <memory>
#include <vector>
#include "deck_info.h"
#include "tag.h"

namespace Sqlite {
class DB;
class Stmt;
}

class Storage {
 public:
  Storage(const QString& filename);
  virtual ~Storage() = 0;
  const DeckInfo& getDeckInfo() const;
  std::vector<Tag> getTags();

 protected:
  std::unique_ptr<Sqlite::DB> db;
  std::unique_ptr<Sqlite::Stmt> get_all_tags;
  DeckInfo deck_info;

  Storage() = default;
  void prepare();
};
