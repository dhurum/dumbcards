#pragma once

#include <memory>
#include <string>
#include <unordered_map>

struct sqlite3;
struct sqlite3_stmt;

namespace Sqlite {
class Stmt;

class DB {
 public:
  DB(const std::string &name);
  ~DB();
  std::unique_ptr<Stmt> prepare(const std::string &query);
  std::unique_ptr<Stmt> exec(const std::string &query);
  void transaction();
  void commit();
  void rollback();
  int64_t lastInsertRowid();

 private:
  sqlite3 *db;

  inline void error(const std::string &msg) const;
};

class Stmt {
 public:
  ~Stmt();
  void bind(const std::string &param, int value);
  void bind(const std::string &param, int64_t value);
  void bind(const std::string &param, const std::string &value);
  void bind(const std::string &param, const void *value, size_t len);
  void exec();
  bool next();
  bool active();
  template <typename T> T get(int index);

 private:
  sqlite3_stmt *stmt = nullptr;
  std::string query;
  bool reseted = true;
  bool is_active = false;
  bool pending_data = false;
  std::unordered_map<std::string, int> param_indexes;

  Stmt(const std::string &query, sqlite3 *db);
  inline void error(const std::string &msg) const;
  inline int paramIndex(const std::string &param);
  inline void checkBind(const std::string &param, int ret);
  bool step();
  void reset();
  inline void tryReset();
  inline void checkGet(int index);

  friend std::unique_ptr<Stmt> std::make_unique<Stmt>(const std::string &,
                                                      sqlite3 *&);
};
template <> int Stmt::get(int index);
template <> int64_t Stmt::get(int index);
template <> std::string Stmt::get(int index);
template <> std::pair<const void *, size_t> Stmt::get(int index);
template <> bool Stmt::get(int index);
}
