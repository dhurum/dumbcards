/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "table_card.h"
#include <QObject>
#include <QString>
#include "qt_helpers.h"

TableCard::TableCard(int64_t id, const std::string &bytes,
                     const std::vector<int64_t> &tags, int correct)
    : Card(id, correct, tags) {
  if (!data.ParseFromString(bytes)) {
    throw std::runtime_error(QObject::tr("Can't parse card").toStdString());
  }
}
TableCard::TableCard(int64_t id, const QString &title,
                     const std::vector<std::vector<QString>> &table,
                     const QString &appendix, const std::vector<int64_t> &tags)
    : Card(id, 0, tags) {
  data.set_title(title.toStdString());
  data.set_appendix(appendix.toStdString());

  for (const auto &row : table) {
    auto new_row = data.add_table();
    for (const auto &cell : row) {
      new_row->add_cell(cell.toStdString());
    }
  }
}

const QString TableCard::getName() const {
  return data.title().c_str();
}

size_t TableCard::rowsNum() const {
  return data.table_size();
}

size_t TableCard::colsNum() const {
  return data.table(0).cell_size();
}

const QString TableCard::getCell(size_t row, size_t col) const {
  return data.table(row).cell(col).c_str();
}

const QString TableCard::getAppendix() const {
  return data.appendix().c_str();
}

std::string TableCard::serialize() const {
  std::string serialized;

  if (!data.SerializeToString(&serialized)) {
    throw std::runtime_error(QObject::tr("Can't serialize card").toStdString());
  }
  return serialized;
}

int TableCard::getType() const {
  return Card::TypeTable;
}

bool TableCard::matchAll(const QString &match, bool match_case,
                         bool whole_text) const {
  if (matchString(getName(), match, match_case, whole_text)
      || matchString(getAppendix(), match, match_case, whole_text)) {
    return true;
  }
  for (size_t i = 0; i < rowsNum(); ++i) {
    for (size_t j = 0; j < colsNum(); ++j) {
      if (matchString(getCell(i, j), match, match_case, whole_text)) {
        return true;
      }
    }
  }
  return false;
}
