/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "storage.h"
#include <QFile>
#include <QObject>
#include <stdexcept>
#include "sqlite_wrapper.h"

Storage::Storage(const QString &filename) {
  if (!QFile::exists(filename)) {
    throw std::runtime_error(
        QObject::tr("Can't find file '%1'").arg(filename).toStdString());
  }
  db = std::make_unique<Sqlite::DB>(filename.toStdString());

  auto get_deck_info = db->exec("select name from deck_info");
  if (!get_deck_info->next()) {
    throw std::runtime_error(QObject::tr("No deck info found").toStdString());
  }
  deck_info.name = get_deck_info->get<std::string>(0).c_str();

  auto get_word_fields =
      db->exec("select name, is_key, rowid from word_field_info");
  while (get_word_fields->next()) {
    deck_info.word_fields.push_back(WordFieldInfo{
        get_word_fields->get<std::string>(0).c_str(),
        get_word_fields->get<int>(1), get_word_fields->get<int64_t>(2)});
  }

  prepare();
}

Storage::~Storage() = default;

void Storage::prepare() {
  get_all_tags = db->prepare("select rowid, name from tag");
}

const DeckInfo &Storage::getDeckInfo() const {
  return deck_info;
}

std::vector<Tag> Storage::getTags() {
  std::vector<Tag> tags;

  get_all_tags->exec();
  while (get_all_tags->next()) {
    tags.push_back({get_all_tags->get<int64_t>(0),
                    get_all_tags->get<std::string>(1).c_str()});
  }

  return tags;
}
