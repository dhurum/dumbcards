/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include <gtest/gtest.h>
#include "sqlite_wrapper.h"
#include "storage.h"

class ConcreteStorage : public Storage {
 public:
  ConcreteStorage(const QString& filename) : Storage(filename){};
  ~ConcreteStorage() = default;
};

class TestStorage : public testing::Test {
 public:
  TestStorage();

  Sqlite::DB db;
  std::unique_ptr<ConcreteStorage> storage;
};

TestStorage::TestStorage()
    : db("lib_storage_test.dc"),
      storage(new ConcreteStorage("lib_storage_test.dc")) {}

TEST_F(TestStorage, storage) {
  auto deck_info = storage->getDeckInfo();

  ASSERT_EQ(deck_info.name.toStdString(), "test");

  ASSERT_EQ(deck_info.word_fields.size(), 2);
  ASSERT_EQ(deck_info.word_fields[0].name, "f1");
  ASSERT_EQ(deck_info.word_fields[0].is_key, 1);
  ASSERT_EQ(deck_info.word_fields[1].name, "f2");
  ASSERT_EQ(deck_info.word_fields[1].is_key, 0);
}

TEST_F(TestStorage, getTags) {
  auto tags = storage->getTags();

  ASSERT_EQ(tags[0].name, "tag1");
  ASSERT_EQ(tags[1].name, "tag2");

  ASSERT_NE(tags[0].id, tags[1].id);
}
