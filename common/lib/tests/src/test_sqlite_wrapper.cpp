/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include <gtest/gtest.h>
#include "sqlite_wrapper.h"

TEST(DB, Connect) {
  Sqlite::DB db("lib_sqlite_test.dc");
}

TEST(DB, Prepare) {
  Sqlite::DB db("lib_sqlite_test.dc");
  auto stmt = db.prepare("select * from test");
}

TEST(DB, Exec) {
  Sqlite::DB db("lib_sqlite_test.dc");
  db.exec("delete from test");
  db.exec("insert into test values (10, \"text\", \"blob\")");

  auto stmt = db.exec("select * from test");
  ASSERT_TRUE(stmt->next());
}

TEST(DB, Transaction) {
  Sqlite::DB db("lib_sqlite_test.dc");
  db.exec("delete from test");
  db.transaction();
  db.exec("insert into test values (10, \"text\", \"blob\")");
  db.commit();

  auto stmt = db.prepare("select * from test");
  stmt->exec();
  ASSERT_TRUE(stmt->next());
}

TEST(DB, RollbackTransaction) {
  Sqlite::DB db("lib_sqlite_test.dc");
  db.exec("delete from test");
  db.transaction();
  db.exec("insert into test values (10, \"text\", \"blob\")");
  db.rollback();

  auto stmt = db.prepare("select * from test");
  stmt->exec();
  ASSERT_FALSE(stmt->next());
}

TEST(DB, DBCloseTransaction) {
  auto db = std::make_unique<Sqlite::DB>("lib_sqlite_test.dc");
  db->exec("delete from test");
  db->transaction();
  db->exec("insert into test values (10, \"text\", \"blob\")");
  db.reset();

  db = std::make_unique<Sqlite::DB>("lib_sqlite_test.dc");
  auto stmt = db->prepare("select * from test");
  stmt->exec();
  ASSERT_FALSE(stmt->next());
}

TEST(DB, LastInsertRowid) {
  Sqlite::DB db("lib_sqlite_test.dc");
  db.exec("delete from test");
  db.exec("insert into test values (10, \"text\", \"blob\")");
  ASSERT_EQ(db.lastInsertRowid(), 1);
}

class Stmt : public testing::Test {
 public:
  Stmt();

  Sqlite::DB db;
};

Stmt::Stmt() : db("lib_sqlite_test.dc") {
  db.exec("delete from test");
  db.exec("insert into test values (10, \"text\", \"blob\")");
}

TEST_F(Stmt, Exec) {
  auto stmt = db.prepare("select * from test");
  stmt->exec();
  ASSERT_TRUE(stmt->active());
  ASSERT_TRUE(stmt->next());
}

TEST_F(Stmt, EmptyResult) {
  auto stmt = db.prepare("select * from test where f_int = 100");
  ASSERT_FALSE(stmt->active());
  ASSERT_FALSE(stmt->next());
}

TEST_F(Stmt, GetInt) {
  auto stmt = db.prepare("select * from test");
  stmt->exec();
  ASSERT_TRUE(stmt->next());
  ASSERT_EQ(stmt->get<int>(0), 10);
}

TEST_F(Stmt, GetInt64) {
  auto stmt = db.prepare("select * from test");
  stmt->exec();
  ASSERT_TRUE(stmt->next());
  ASSERT_EQ(stmt->get<int64_t>(0), 10);
}

TEST_F(Stmt, GetText) {
  auto stmt = db.prepare("select * from test");
  stmt->exec();
  ASSERT_TRUE(stmt->next());
  ASSERT_EQ(stmt->get<std::string>(1), "text");
}

TEST_F(Stmt, GetBlob) {
  auto stmt = db.prepare("select * from test");
  stmt->exec();
  ASSERT_TRUE(stmt->next());
  auto blob_val = stmt->get<std::pair<const void *, size_t>>(2);
  ASSERT_STREQ(static_cast<const char *>(blob_val.first), "blob");
  ASSERT_EQ(blob_val.second, 4);
}

TEST_F(Stmt, GetBool) {
  db.exec("insert into test values (1, \"text2\", \"\")");
  db.exec("insert into test values (0, \"text3\", \"\")");
  auto stmt = db.prepare("select * from test where f_text = \"text2\"");
  stmt->exec();
  ASSERT_TRUE(stmt->next());
  ASSERT_TRUE(stmt->get<int>(0));

  stmt = db.prepare("select * from test where f_text = \"text3\"");
  stmt->exec();
  ASSERT_TRUE(stmt->next());
  ASSERT_FALSE(stmt->get<int>(0));
}

TEST_F(Stmt, GetInvalidIndex) {
  auto stmt = db.prepare("select * from test");
  stmt->exec();
  ASSERT_TRUE(stmt->next());

  bool error_thrown = false;
  try {
    stmt->get<int>(100);
  } catch (...) {
    error_thrown = true;
  }
  ASSERT_TRUE(error_thrown);
}

TEST_F(Stmt, BindIncorrect) {
  bool error_thrown = false;
  auto stmt = db.prepare("select * from test");
  try {
    stmt->bind(":f_incorrect", 10);
  } catch (...) {
    error_thrown = true;
  }
  ASSERT_TRUE(error_thrown);
}

TEST_F(Stmt, BindInt) {
  auto add_stmt =
      db.prepare("insert into test values (:f_int, \"text2\", \"\")");
  int int_val = 20;
  add_stmt->bind(":f_int", int_val);
  add_stmt->exec();

  auto get_stmt = db.prepare("select * from test where f_text = \"text2\"");
  get_stmt->exec();
  ASSERT_TRUE(get_stmt->next());
  ASSERT_EQ(get_stmt->get<int>(0), 20);
}

TEST_F(Stmt, BindInt64) {
  auto add_stmt =
      db.prepare("insert into test values (:f_int, \"text2\", \"\")");
  int64_t int_val = 20;
  add_stmt->bind(":f_int", int_val);
  add_stmt->exec();

  auto get_stmt = db.prepare("select * from test where f_text = \"text2\"");
  get_stmt->exec();
  ASSERT_TRUE(get_stmt->next());
  ASSERT_EQ(get_stmt->get<int64_t>(0), 20);
}

TEST_F(Stmt, BindString) {
  auto add_stmt = db.prepare("insert into test values (20, :f_text, \"\")");
  std::string text_val("text2");
  add_stmt->bind(":f_text", text_val);
  add_stmt->exec();

  auto get_stmt = db.prepare("select * from test where f_int = 20");
  get_stmt->exec();
  ASSERT_TRUE(get_stmt->next());
  ASSERT_EQ(get_stmt->get<std::string>(1), "text2");
}

TEST_F(Stmt, BindCString) {
  auto add_stmt = db.prepare("insert into test values (20, :f_text, \"\")");
  add_stmt->bind(":f_text", "text2");
  add_stmt->exec();

  auto get_stmt = db.prepare("select * from test where f_int = 20");
  get_stmt->exec();
  ASSERT_TRUE(get_stmt->next());
  ASSERT_EQ(get_stmt->get<std::string>(1), "text2");
}

TEST_F(Stmt, BindBlob) {
  auto add_stmt =
      db.prepare("insert into test values (20, \"text2\", :f_blob)");
  add_stmt->bind(":f_blob", "blob2", 5);
  add_stmt->exec();

  auto get_stmt = db.prepare("select * from test where f_int = 20");
  get_stmt->exec();
  ASSERT_TRUE(get_stmt->next());
  auto blob_val = get_stmt->get<std::pair<const void *, size_t>>(2);
  ASSERT_STREQ(static_cast<const char *>(blob_val.first), "blob2");
  ASSERT_EQ(blob_val.second, 5);
}

TEST_F(Stmt, BindBool) {
  auto add_stmt =
      db.prepare("insert into test values (:f_bool, :f_text, \"\")");
  add_stmt->bind(":f_bool", true);
  add_stmt->bind(":f_text", "text2");
  add_stmt->exec();

  auto get_stmt = db.prepare("select * from test where f_text = :f_text");
  get_stmt->bind(":f_text", "text2");
  get_stmt->exec();
  ASSERT_TRUE(get_stmt->next());
  ASSERT_TRUE(get_stmt->get<int>(0));

  add_stmt->bind(":f_bool", false);
  add_stmt->bind(":f_text", "text3");
  add_stmt->exec();

  get_stmt->bind(":f_text", "text3");
  get_stmt->exec();
  ASSERT_TRUE(get_stmt->next());
  ASSERT_FALSE(get_stmt->get<int>(0));
}

TEST_F(Stmt, Next) {
  db.exec("insert into test values (20, \"text2\", \"blob2\")");

  auto stmt = db.prepare("select * from test");
  stmt->exec();
  ASSERT_TRUE(stmt->next());
  auto int_val = stmt->get<int>(0);
  ASSERT_EQ(int_val, 10);

  ASSERT_TRUE(stmt->next());
  int_val = stmt->get<int>(0);
  ASSERT_EQ(int_val, 20);

  ASSERT_FALSE(stmt->next());
}

TEST_F(Stmt, NoFirstNext) {
  db.exec("insert into test values (20, \"text2\", \"blob2\")");

  auto stmt = db.prepare("select * from test");
  stmt->exec();
  auto int_val = stmt->get<int>(0);
  ASSERT_EQ(int_val, 10);

  ASSERT_TRUE(stmt->next());
  int_val = stmt->get<int>(0);
  ASSERT_EQ(int_val, 20);

  ASSERT_FALSE(stmt->next());
}
