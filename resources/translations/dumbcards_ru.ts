<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>Application</name>
    <message>
        <location filename="../../common/gui/src/application.cpp" line="34"/>
        <source>Critical Error</source>
        <translation>Критическая Ошибка</translation>
    </message>
</context>
<context>
    <name>CardEditor</name>
    <message>
        <location filename="../../editor/src/card_editor.cpp" line="43"/>
        <source>Word</source>
        <translation>Слово</translation>
    </message>
    <message>
        <location filename="../../editor/src/card_editor.cpp" line="44"/>
        <source>Table</source>
        <translation>Таблица</translation>
    </message>
    <message>
        <location filename="../../editor/src/card_editor.cpp" line="45"/>
        <source>Fact</source>
        <translation>Факт</translation>
    </message>
    <message>
        <location filename="../../editor/src/card_editor.cpp" line="64"/>
        <source>New Card</source>
        <translation>Новая Карточка</translation>
    </message>
    <message>
        <location filename="../../editor/src/card_editor.cpp" line="72"/>
        <source>Edit Card</source>
        <translation>Редактирование Карточки</translation>
    </message>
    <message>
        <location filename="../../editor/src/card_editor.cpp" line="78"/>
        <source>Tags:</source>
        <translation>Тэги:</translation>
    </message>
</context>
<context>
    <name>CardFindDialog</name>
    <message>
        <location filename="../../editor/src/card_find_dialog.cpp" line="35"/>
        <source>Find Cards</source>
        <translation>Поиск Карточек</translation>
    </message>
    <message>
        <location filename="../../editor/src/card_find_dialog.cpp" line="39"/>
        <source>Search all fields</source>
        <translation>Искать во всех полях</translation>
    </message>
    <message>
        <location filename="../../editor/src/card_find_dialog.cpp" line="42"/>
        <source>With tags:</source>
        <translation>Содержащие тэги:</translation>
    </message>
    <message>
        <location filename="../../editor/src/card_find_dialog.cpp" line="47"/>
        <source>With any of the tags</source>
        <translation>С любыми тэгами</translation>
    </message>
    <message>
        <location filename="../../editor/src/card_find_dialog.cpp" line="48"/>
        <source>With all of the tags</source>
        <translation>Со всеми тегами</translation>
    </message>
</context>
<context>
    <name>CardWidget</name>
    <message>
        <location filename="../../client/src/card_widget.cpp" line="47"/>
        <source>Show</source>
        <translation>Показать</translation>
    </message>
    <message>
        <location filename="../../client/src/card_widget.cpp" line="56"/>
        <source>Correct</source>
        <translation>Правильно</translation>
    </message>
    <message>
        <location filename="../../client/src/card_widget.cpp" line="61"/>
        <source>Incorrect</source>
        <translation>Неправильно</translation>
    </message>
</context>
<context>
    <name>CardsFilter</name>
    <message>
        <location filename="../../client/src/cards_filter.cpp" line="40"/>
        <source>Cards Filter</source>
        <translation>Фильтр Карточек</translation>
    </message>
    <message>
        <location filename="../../client/src/cards_filter.cpp" line="42"/>
        <source>Show cards:</source>
        <translation>Показать карточки:</translation>
    </message>
    <message>
        <location filename="../../client/src/cards_filter.cpp" line="44"/>
        <source>Correctly answered</source>
        <translation>Корректно отвеченные</translation>
    </message>
    <message>
        <location filename="../../client/src/cards_filter.cpp" line="48"/>
        <source>In random order</source>
        <translation>В случайном порядке</translation>
    </message>
    <message>
        <location filename="../../client/src/cards_filter.cpp" line="61"/>
        <source>minutes</source>
        <translation>минут</translation>
    </message>
    <message>
        <location filename="../../client/src/cards_filter.cpp" line="62"/>
        <source>hours</source>
        <translation>часов</translation>
    </message>
    <message>
        <location filename="../../client/src/cards_filter.cpp" line="63"/>
        <source>days</source>
        <translation>дней</translation>
    </message>
    <message>
        <location filename="../../client/src/cards_filter.cpp" line="53"/>
        <source>Answered earlier than:</source>
        <translation>Отвеченные ранее, чем:</translation>
    </message>
    <message>
        <location filename="../../client/src/cards_filter.cpp" line="68"/>
        <source>Cards number limit:</source>
        <translation>Ограничение количества карточек:</translation>
    </message>
    <message>
        <location filename="../../client/src/cards_filter.cpp" line="74"/>
        <source>With tags:</source>
        <translation>Содержащие тэги:</translation>
    </message>
    <message>
        <location filename="../../client/src/cards_filter.cpp" line="79"/>
        <source>With any of the tags</source>
        <translation>С любыми тэгами</translation>
    </message>
    <message>
        <location filename="../../client/src/cards_filter.cpp" line="80"/>
        <source>With all of the tags</source>
        <translation>Со всеми тегами</translation>
    </message>
    <message>
        <location filename="../../client/src/cards_filter.cpp" line="117"/>
        <location filename="../../client/src/cards_filter.cpp" line="129"/>
        <source>Invalid Data</source>
        <translation>Неправильные Данные</translation>
    </message>
    <message>
        <location filename="../../client/src/cards_filter.cpp" line="118"/>
        <location filename="../../client/src/cards_filter.cpp" line="130"/>
        <source>Please, set correct number</source>
        <translation>Пожалуйста, введите корректное число</translation>
    </message>
</context>
<context>
    <name>DeckEditor</name>
    <message>
        <location filename="../../editor/src/deck_editor.cpp" line="43"/>
        <source>Name:</source>
        <translation>Название:</translation>
    </message>
    <message>
        <location filename="../../editor/src/deck_editor.cpp" line="47"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../../editor/src/deck_editor.cpp" line="54"/>
        <source>Edit Deck</source>
        <translation>Редактирование Колоды</translation>
    </message>
    <message>
        <location filename="../../editor/src/deck_editor.cpp" line="56"/>
        <source>New Deck</source>
        <translation>Новая Колода</translation>
    </message>
    <message>
        <location filename="../../editor/src/deck_editor.cpp" line="82"/>
        <source>New Deck File</source>
        <translation>Файл Новой Колоды</translation>
    </message>
    <message>
        <location filename="../../editor/src/deck_editor.cpp" line="82"/>
        <source>Dumbcards Deck (*.dc)</source>
        <translation>Колода Dumbcards (*.dc)</translation>
    </message>
    <message>
        <location filename="../../editor/src/deck_editor.cpp" line="90"/>
        <location filename="../../editor/src/deck_editor.cpp" line="97"/>
        <source>Invalid Data</source>
        <translation>Неправильные Данные</translation>
    </message>
    <message>
        <location filename="../../editor/src/deck_editor.cpp" line="91"/>
        <source>Please, set name</source>
        <translation>Пожалуйста, введите название</translation>
    </message>
    <message>
        <location filename="../../editor/src/deck_editor.cpp" line="98"/>
        <source>Please, set file</source>
        <translation>Пожалуйста, выберите файл</translation>
    </message>
</context>
<context>
    <name>FactCardEditor</name>
    <message>
        <location filename="../../editor/src/fact_card_editor.cpp" line="37"/>
        <source>Title:</source>
        <translation>Заголовок:</translation>
    </message>
    <message>
        <location filename="../../editor/src/fact_card_editor.cpp" line="42"/>
        <source>Description:</source>
        <translation>Описание:</translation>
    </message>
    <message>
        <location filename="../../editor/src/fact_card_editor.cpp" line="62"/>
        <location filename="../../editor/src/fact_card_editor.cpp" line="70"/>
        <source>Invalid Data</source>
        <translation>Неправильные Данные</translation>
    </message>
    <message>
        <location filename="../../editor/src/fact_card_editor.cpp" line="63"/>
        <source>Please, fill title</source>
        <translation>Пожалуйста, введите заголовок</translation>
    </message>
    <message>
        <location filename="../../editor/src/fact_card_editor.cpp" line="71"/>
        <source>Please, fill description</source>
        <translation>Пожалуйста, введите описание</translation>
    </message>
</context>
<context>
    <name>FindDialog</name>
    <message>
        <location filename="../../editor/src/find_dialog.cpp" line="41"/>
        <source>Find:</source>
        <translation>Поиск:</translation>
    </message>
    <message>
        <location filename="../../editor/src/find_dialog.cpp" line="45"/>
        <source>Match case</source>
        <translation>Учитывать регистр</translation>
    </message>
    <message>
        <location filename="../../editor/src/find_dialog.cpp" line="48"/>
        <source>Match whole text</source>
        <translation>Искать точное совпадение</translation>
    </message>
    <message>
        <location filename="../../editor/src/find_dialog.cpp" line="58"/>
        <source>Find Next</source>
        <translation>Найти Следующий</translation>
    </message>
    <message>
        <location filename="../../editor/src/find_dialog.cpp" line="62"/>
        <source>Find Previous</source>
        <translation>Найти Предыдущий</translation>
    </message>
    <message>
        <location filename="../../editor/src/find_dialog.cpp" line="66"/>
        <source>Select All</source>
        <translation>Выделить Все</translation>
    </message>
    <message>
        <location filename="../../editor/src/find_dialog.cpp" line="70"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../client/src/main_window.cpp" line="38"/>
        <location filename="../../editor/src/main_window.cpp" line="44"/>
        <source>&amp;Deck</source>
        <translation>К&amp;олода</translation>
    </message>
    <message>
        <location filename="../../client/src/main_window.cpp" line="39"/>
        <source>&amp;Cards</source>
        <translation>&amp;Карточки</translation>
    </message>
    <message>
        <location filename="../../client/src/main_window.cpp" line="42"/>
        <location filename="../../editor/src/main_window.cpp" line="51"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="../../client/src/main_window.cpp" line="44"/>
        <source>Open Deck</source>
        <translation>Открыть Колоду</translation>
    </message>
    <message>
        <location filename="../../client/src/main_window.cpp" line="48"/>
        <source>Filter</source>
        <translation>Фильтр</translation>
    </message>
    <message>
        <location filename="../../client/src/main_window.cpp" line="50"/>
        <source>Filter Cards</source>
        <translation>Фильтровать Карточки</translation>
    </message>
    <message>
        <location filename="../../client/src/main_window.cpp" line="54"/>
        <source>Mark the card as correct</source>
        <translation>Отметить карточку как правильную</translation>
    </message>
    <message>
        <location filename="../../client/src/main_window.cpp" line="57"/>
        <source>Mark current card as answered fully correct</source>
        <translation>Отметить текущую карточку как полностью правильную</translation>
    </message>
    <message>
        <location filename="../../client/src/main_window.cpp" line="67"/>
        <location filename="../../editor/src/main_window.cpp" line="180"/>
        <source>Open Deck File</source>
        <translation>Открыть Файл Колоды</translation>
    </message>
    <message>
        <location filename="../../client/src/main_window.cpp" line="68"/>
        <location filename="../../editor/src/main_window.cpp" line="181"/>
        <source>Dumbcards Deck (*.dc)</source>
        <translation>Колода Dumbcards (*.dc)</translation>
    </message>
    <message>
        <location filename="../../client/src/main_window.cpp" line="138"/>
        <source>Mark Card As Correct</source>
        <translation>Отметить Карточку Как Правильную</translation>
    </message>
    <message>
        <location filename="../../client/src/main_window.cpp" line="139"/>
        <source>Are you sure you want to mark this card as correct?</source>
        <translation>Вы уверены, что хотите отметить эту карточку как правильную?</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="46"/>
        <source>New</source>
        <translation>Новая</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="47"/>
        <source>Create new deck</source>
        <translation>Создать новую колоду</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="53"/>
        <source>Open deck</source>
        <translation>Открыть колоду</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="57"/>
        <source>Edit</source>
        <translation>Править</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="59"/>
        <source>Edit deck</source>
        <translation>Править колоду</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="64"/>
        <source>Export in JSON</source>
        <translation>Экспортировать в JSON</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="65"/>
        <source>Export deck in JSON</source>
        <translation>Экспортировать колоду в JSON</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="70"/>
        <source>Card</source>
        <translation>Карточка</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="73"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="77"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="81"/>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="93"/>
        <source>Cards</source>
        <translation>Карточки</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="98"/>
        <source>Tags</source>
        <translation>Тэги</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="110"/>
        <source>Create new card</source>
        <translation>Создать новую карточку</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="113"/>
        <source>Delete card</source>
        <translation>Удалить карточку</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="117"/>
        <source>Find card</source>
        <translation>Найти карточку</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="123"/>
        <source>Create new tag</source>
        <translation>Создать новый тэг</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="126"/>
        <source>Delete tag</source>
        <translation>Удалить тэг</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="130"/>
        <source>Find tag</source>
        <translation>Найти тэг</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="197"/>
        <source>Export To</source>
        <translation>Экспортировать В</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="197"/>
        <source>JSON (*.json)</source>
        <translation>JSON (*.json)</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="251"/>
        <source>Delete Cards</source>
        <translation>Удалить Карточки</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="252"/>
        <source>Are you sure you want to delete selected cards?</source>
        <translation>Вы уверены, что хотите удалить выбранные карточки?</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="335"/>
        <source>Delete Tags</source>
        <translation>Удалить Тэги</translation>
    </message>
    <message>
        <location filename="../../editor/src/main_window.cpp" line="336"/>
        <source>Are you sure you want to delete selected tags?</source>
        <translation>Вы уверены, что хотите удалить выбранные тэги?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../common/gui/src/main_template.h" line="53"/>
        <source>Deck file</source>
        <translation>Файл колоды</translation>
    </message>
    <message>
        <location filename="../../common/gui/src/main_template.h" line="71"/>
        <source>Critical Error</source>
        <translation>Критическая Ошибка</translation>
    </message>
    <message>
        <location filename="../../common/lib/src/fact_card.cpp" line="31"/>
        <location filename="../../common/lib/src/table_card.cpp" line="31"/>
        <location filename="../../common/lib/src/word_card.cpp" line="32"/>
        <source>Can&apos;t parse card</source>
        <translation>Не могу прочитать карточку</translation>
    </message>
    <message>
        <location filename="../../common/lib/src/fact_card.cpp" line="53"/>
        <location filename="../../common/lib/src/table_card.cpp" line="73"/>
        <location filename="../../common/lib/src/word_card.cpp" line="64"/>
        <source>Can&apos;t serialize card</source>
        <translation>Не могу записать карточку</translation>
    </message>
    <message>
        <location filename="../../common/lib/src/storage.cpp" line="31"/>
        <source>Can&apos;t find file &apos;%1&apos;</source>
        <translation>Не могу найти файл &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../common/lib/src/storage.cpp" line="37"/>
        <source>No deck info found</source>
        <translation>Не найдено информации о колоде</translation>
    </message>
    <message>
        <location filename="../../editor/src/editor_storage.cpp" line="41"/>
        <source>Can&apos;t remove existing file &apos;%1&apos;</source>
        <translation>Не могу удалить существующий файл &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>TableCardEditor</name>
    <message>
        <location filename="../../editor/src/table_card_editor.cpp" line="39"/>
        <source>Title:</source>
        <translation>Заголовок:</translation>
    </message>
    <message>
        <location filename="../../editor/src/table_card_editor.cpp" line="45"/>
        <source>Add Row</source>
        <translation>Добавить Строку</translation>
    </message>
    <message>
        <location filename="../../editor/src/table_card_editor.cpp" line="49"/>
        <source>Delete Row</source>
        <translation>Удалить Строку</translation>
    </message>
    <message>
        <location filename="../../editor/src/table_card_editor.cpp" line="53"/>
        <source>Add Column</source>
        <translation>Добавить Колонку</translation>
    </message>
    <message>
        <location filename="../../editor/src/table_card_editor.cpp" line="57"/>
        <source>Delete Column</source>
        <translation>Удалить Колонку</translation>
    </message>
    <message>
        <location filename="../../editor/src/table_card_editor.cpp" line="66"/>
        <source>Appendix:</source>
        <translation>Дополнение:</translation>
    </message>
    <message>
        <location filename="../../editor/src/table_card_editor.cpp" line="163"/>
        <location filename="../../editor/src/table_card_editor.cpp" line="181"/>
        <source>Invalid Data</source>
        <translation>Неправильные Данные</translation>
    </message>
    <message>
        <location filename="../../editor/src/table_card_editor.cpp" line="164"/>
        <source>Please, fill title</source>
        <translation>Пожалуйста, введите заголовок</translation>
    </message>
    <message>
        <location filename="../../editor/src/table_card_editor.cpp" line="182"/>
        <source>Please, fill at least one cell</source>
        <translation>Пожалуйста, заполните хотя бы одну ячейку</translation>
    </message>
</context>
<context>
    <name>TagEditor</name>
    <message>
        <location filename="../../editor/src/tag_editor.cpp" line="40"/>
        <source>Name:</source>
        <translation>Название:</translation>
    </message>
    <message>
        <location filename="../../editor/src/tag_editor.cpp" line="45"/>
        <source>Edit Tag</source>
        <translation>Редактирование Тэга</translation>
    </message>
    <message>
        <location filename="../../editor/src/tag_editor.cpp" line="47"/>
        <source>New Tag</source>
        <translation>Новый Тэг</translation>
    </message>
    <message>
        <location filename="../../editor/src/tag_editor.cpp" line="66"/>
        <source>Invalid Data</source>
        <translation>Неправильные Данные</translation>
    </message>
    <message>
        <location filename="../../editor/src/tag_editor.cpp" line="67"/>
        <source>Please, set name</source>
        <translation>Пожалуйста, введите название</translation>
    </message>
</context>
<context>
    <name>TagFindDialog</name>
    <message>
        <location filename="../../editor/src/tag_find_dialog.cpp" line="30"/>
        <source>Find Tags</source>
        <translation>Поиск Тэгов</translation>
    </message>
</context>
<context>
    <name>WordCardEditor</name>
    <message>
        <location filename="../../editor/src/word_card_editor.cpp" line="65"/>
        <location filename="../../editor/src/word_card_editor.cpp" line="77"/>
        <source>Invalid Data</source>
        <translation>Неправильные Данные</translation>
    </message>
    <message>
        <location filename="../../editor/src/word_card_editor.cpp" line="66"/>
        <source>Please, fill key fields</source>
        <translation>Пожалуйста, заполните ключевые поля</translation>
    </message>
    <message>
        <location filename="../../editor/src/word_card_editor.cpp" line="78"/>
        <source>Please, fill at least two fields</source>
        <translation>Пожалуйста, заполните хотя бы два поля</translation>
    </message>
</context>
<context>
    <name>WordFields</name>
    <message>
        <location filename="../../editor/src/word_fields.cpp" line="33"/>
        <source>Word fields</source>
        <translation>Поля Слова</translation>
    </message>
    <message>
        <location filename="../../editor/src/word_fields.cpp" line="35"/>
        <source>Add field</source>
        <translation>Добавить поле</translation>
    </message>
    <message>
        <location filename="../../editor/src/word_fields.cpp" line="41"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../editor/src/word_fields.cpp" line="45"/>
        <source>Key</source>
        <translation>Ключевое</translation>
    </message>
    <message>
        <location filename="../../editor/src/word_fields.cpp" line="63"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../../editor/src/word_fields.cpp" line="131"/>
        <location filename="../../editor/src/word_fields.cpp" line="143"/>
        <location filename="../../editor/src/word_fields.cpp" line="152"/>
        <source>Invalid Data</source>
        <translation>Неправильные Данные</translation>
    </message>
    <message>
        <location filename="../../editor/src/word_fields.cpp" line="132"/>
        <source>Please, set field name</source>
        <translation>Пожалуйста, введите название</translation>
    </message>
    <message>
        <location filename="../../editor/src/word_fields.cpp" line="144"/>
        <source>Please, mark at least one field as a key</source>
        <translation>Пожалуйста, пометьте хотя бы одно поле как ключевое</translation>
    </message>
    <message>
        <location filename="../../editor/src/word_fields.cpp" line="153"/>
        <source>Please, add at least one more field or remove existing one</source>
        <translation>Пожалуйста, добавьте еще хотя бы одно поле, или удалите существующее</translation>
    </message>
</context>
</TS>
