/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include <gtest/gtest.h>
#include "client_storage.h"
#include "sqlite_wrapper.h"

class TestStorage : public testing::Test {
 public:
  TestStorage();

  Sqlite::DB db;
  std::unique_ptr<ClientStorage> storage;
};

TestStorage::TestStorage()
    : db("client_storage_test.dc"),
      storage(new ClientStorage("client_storage_test.dc")) {}

TEST_F(TestStorage, storage) {
  auto deck_info = storage->getDeckInfo();

  ASSERT_EQ(deck_info.name.toStdString(), "test");

  ASSERT_EQ(deck_info.word_fields.size(), 2);
  ASSERT_EQ(deck_info.word_fields[0].name, "f1");
  ASSERT_EQ(deck_info.word_fields[0].is_key, 1);
  ASSERT_EQ(deck_info.word_fields[1].name, "f2");
  ASSERT_EQ(deck_info.word_fields[1].is_key, 0);
}

TEST_F(TestStorage, filter) {
  storage->setFilter({true, false, 10, Filter::Minutes, 20, {}, false});

  auto filter = storage->getFilter();
  ASSERT_EQ(filter.correct, true);
  ASSERT_EQ(filter.random, false);
  ASSERT_EQ(filter.time_ago, 10);
  ASSERT_EQ(filter.time_units, Filter::Minutes);
  ASSERT_EQ(filter.limit, 20);
  ASSERT_EQ(filter.tags.size(), 0);
  ASSERT_EQ(filter.all_tags, false);
}
