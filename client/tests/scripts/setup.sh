#!/bin/bash

sqlite3 client_storage_test.dc "create table deck_info(name text, use_words integer)"
sqlite3 client_storage_test.dc "create table word_field_info(name text, is_key integer)"
sqlite3 client_storage_test.dc "create table card_data(type integer, data blob)"
sqlite3 client_storage_test.dc "create table card_instance(data_id integer, field integer, correct integer, last_answered text)"
sqlite3 client_storage_test.dc "create table tag(name text)"
sqlite3 client_storage_test.dc "create table card_tag(data_id integer, tag_id integer)"

sqlite3 client_storage_test.dc  "insert into deck_info (name) values ('test')"
sqlite3 client_storage_test.dc  "insert into word_field_info (name, is_key) values ('f1', 1)"
sqlite3 client_storage_test.dc  "insert into word_field_info (name, is_key) values ('f2', 0)"
