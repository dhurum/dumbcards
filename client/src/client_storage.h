/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <memory>
#include "filter.h"
#include "storage.h"

class Card;

class ClientStorage : public Storage {
 public:
  ClientStorage(const QString &filename);
  ~ClientStorage();
  void setFilter(const Filter &_filter);
  const Filter &getFilter();
  std::unique_ptr<Card> getCard();
  void updateCard(Card &card, bool correct);
  void markCardCorrect(const Card &card);

 private:
  std::unique_ptr<Sqlite::Stmt> get_card;
  std::unique_ptr<Sqlite::Stmt> update_card;
  std::unique_ptr<Sqlite::Stmt> mark_card_correct;
  Filter filter;
};
