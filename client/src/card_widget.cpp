/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "card_widget.h"
#include <QHBoxLayout>
#include <QProgressBar>
#include <QPushButton>
#include <QVBoxLayout>
#include "card.h"
#include "fact_card_widget.h"
#include "qt_helpers.h"
#include "table_card_widget.h"
#include "word_card_widget.h"

CardWidget::CardWidget() {
  auto layout = new QVBoxLayout();
  setLayout(layout);

  word_widget = new WordCardWidget();
  layout->addWidget(word_widget);

  table_widget = new TableCardWidget();
  layout->addWidget(table_widget);

  fact_widget = new FactCardWidget();
  layout->addWidget(fact_widget);
  layout->addStretch();

  show_button = new QPushButton(tr("Show"));
  CONNECT(show_button, clicked, this, showFull);
  layout->addWidget(show_button);
  show_button->hide();

  result_buttons = new QWidget();
  auto result_layout = new QHBoxLayout();
  result_buttons->setLayout(result_layout);

  correct_button = new QPushButton(tr("Correct"));
  CONNECT_FUNCTION(correct_button, clicked,
                   [this]() { emit(this->answered(true)); });
  result_layout->addWidget(correct_button);

  auto incorrect_button = new QPushButton(tr("Incorrect"));
  CONNECT_FUNCTION(incorrect_button, clicked,
                   [this]() { emit(this->answered(false)); });
  result_layout->addWidget(incorrect_button);
  layout->addWidget(result_buttons);

  progress_bar = new QProgressBar();
  progress_bar->setFormat("%v / %m");
  layout->addWidget(progress_bar);

  result_buttons->hide();
}

void CardWidget::showCard(const Card &card, size_t remaining_cards) {
  if (card.getType() == Card::TypeWord) {
    current_card_widget = word_widget;
    word_widget->show();
    table_widget->hide();
    fact_widget->hide();
  } else if (card.getType() == Card::TypeTable) {
    current_card_widget = table_widget;
    word_widget->hide();
    table_widget->show();
    fact_widget->hide();
  } else {
    current_card_widget = fact_widget;
    word_widget->hide();
    table_widget->hide();
    fact_widget->show();
  }

  current_card_widget->showCard(card);
  show_button->show();
  result_buttons->hide();
  show_button->setFocus();
  progress_bar->setValue(cards_num - remaining_cards);
}

void CardWidget::showFull() {
  current_card_widget->showFull();

  show_button->hide();
  result_buttons->show();
  correct_button->setFocus();
}

void CardWidget::setDeckInfo(const DeckInfo &deck_info) {
  word_widget->setDeckInfo(deck_info);
}

void CardWidget::setCardsNum(size_t _cards_num) {
  cards_num = _cards_num;
  progress_bar->setRange(0, cards_num);
}
