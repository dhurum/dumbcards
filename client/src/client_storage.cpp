/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "client_storage.h"
#include <sstream>
#include <stdexcept>
#include <vector>
#include "card.h"
#include "fact_card.h"
#include "filter.h"
#include "sqlite_wrapper.h"
#include "table_card.h"
#include "word_card.h"

constexpr int correct_min = 5;

ClientStorage::ClientStorage(const QString &filename) : Storage(filename) {
  update_card = db->prepare(
      "update card_instance set correct = :correct, last_answered = "
      "datetime('now') where rowid = :rowid");

  setFilter({false, true, 0, Filter::Days, 0, {}, false});
}

ClientStorage::~ClientStorage() = default;

void ClientStorage::setFilter(const Filter &_filter) {
  filter = _filter;
  std::ostringstream get_card_query;
  get_card_query << "select i.rowid, d.type, d.data, i.field, i.correct from "
                    "card_instance i, card_data d";
  if (!filter.tags.empty()) {
    get_card_query << ", card_tag t";
  }
  get_card_query << " where i.data_id = d.rowid";

  if (!filter.tags.empty()) {
    get_card_query << " and t.data_id = d.rowid";
  }

  if (!filter.correct) {
    get_card_query << " and i.correct < " << correct_min;
  }

  if (filter.time_ago > 0) {
    get_card_query
        << " and i.last_answered <= datetime('now', 'start of day', '-"
        << filter.time_ago;
    switch (filter.time_units) {
      case Filter::Minutes:
        get_card_query << " minute')";
        break;
      case Filter::Hours:
        get_card_query << " hour')";
        break;
      case Filter::Days:
        get_card_query << " day')";
        break;
    }
  }

  if (!filter.tags.empty()) {
    get_card_query << " and (t.tag_id in (";
    const char *prefix = "";
    for (auto tag : filter.tags) {
      get_card_query << prefix << tag;
      prefix = ",";
    }
    get_card_query << ")) group by i.rowid";
    if (filter.all_tags) {
      get_card_query << " having count(i.rowid) = " << filter.tags.size();
    }
  }

  if (filter.random) {
    get_card_query << " order by random()";
  }

  if (filter.limit > 0) {
    get_card_query << " limit " << filter.limit;
  }

  get_card = db->prepare(get_card_query.str());
}

const Filter &ClientStorage::getFilter() {
  return filter;
}

std::unique_ptr<Card> ClientStorage::getCard() {
  if (!get_card->active()) {
    get_card->exec();
  }

  if (!get_card->next()) {
    return nullptr;
  }
  auto data = get_card->get<std::pair<const void *, size_t>>(2);
  std::string data_str(static_cast<const char *>(data.first), data.second);
  int type = get_card->get<int>(1);
  switch (type) {
    case Card::TypeWord:
      return std::make_unique<WordCard>(
          get_card->get<int64_t>(0), data_str, std::vector<int64_t>{},
          get_card->get<int>(3), get_card->get<int>(4));
    case Card::TypeTable:
      return std::make_unique<TableCard>(get_card->get<int>(0), data_str,
                                         std::vector<int64_t>{},
                                         get_card->get<int>(4));
    case Card::TypeFact:
      return std::make_unique<FactCard>(get_card->get<int>(0), data_str,
                                        std::vector<int64_t>{},
                                        get_card->get<int>(4));
  }
  return nullptr;
}

void ClientStorage::updateCard(Card &card, bool correct) {
  int new_correct = card.getCorrect() + 1;
  if (!correct) {
    new_correct = 0;
    card.resetCorrect();
  }
  update_card->bind(":correct", new_correct);
  update_card->bind(":rowid", card.getId());
  update_card->exec();
}

void ClientStorage::markCardCorrect(const Card &card) {
  update_card->bind(":correct", correct_min);
  update_card->bind(":rowid", card.getId());
  update_card->exec();
}
