/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <string>
#include <vector>

struct Filter {
  bool correct;
  bool random;
  int time_ago;
  enum TimeUnits { Minutes = 0, Hours, Days } time_units;
  int limit;
  std::vector<int64_t> tags;
  bool all_tags;
};
