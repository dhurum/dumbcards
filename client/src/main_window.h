/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <QMainWindow>
#include <list>
#include <memory>

class QMenu;
class QAction;
class Card;
class ClientStorage;
class CardWidget;

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  MainWindow(const QString &deck_file);
  ~MainWindow();

 private:
  std::unique_ptr<ClientStorage> storage;
  CardWidget *card_widget = nullptr;
  std::list<std::unique_ptr<Card>> cards;
  std::unique_ptr<Card> current_card;
  QMenu *cards_menu;
  QAction *action_mark_correct;

  void readCards();
  void showCard();
  void openFile(const QString &deck_file);
  void updateTitle(const QString &deck_name);

 private slots:
  void openDeck();
  void showFilter();
  void answered(bool correct);
  void markCardCorrect();
};
