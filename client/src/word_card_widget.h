/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <QWidget>
#include <vector>
#include "common_card_widget.h"
#include "deck_info.h"

class QVBoxLayout;
class WQLabel;

class WordCardWidget : public QWidget, public CommonCardWidget {
  Q_OBJECT

 public:
  WordCardWidget();
  void showCard(const Card &card) override;
  void showFull() override;
  void setDeckInfo(const DeckInfo &deck_info);

 private:
  QVBoxLayout *layout;
  const std::vector<WordFieldInfo> *word_fields;
  WQLabel *key_field = nullptr;
  std::vector<WQLabel *> fields;
};
