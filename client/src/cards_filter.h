/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <QDialog>
#include <vector>
#include "filter.h"
#include "tag.h"

class QCheckBox;
class QLineEdit;
class QComboBox;
class CardTagsSelector;

class CardsFilter : public QDialog {
  Q_OBJECT

 public:
  CardsFilter(QWidget *parent, const std::vector<Tag> &tags,
              const Filter &filter);
  Filter getFilter();

 private:
  QCheckBox *correct;
  QCheckBox *random;
  QComboBox *time_units;
  QLineEdit *time;
  QLineEdit *limit;
  QComboBox *tags_type;
  CardTagsSelector *tags_selector;

 private slots:
  void check();
};
