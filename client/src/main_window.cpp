/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "main_window.h"
#include <QAction>
#include <QFileDialog>
#include <QLabel>
#include <QMenuBar>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>
#include "card.h"
#include "card_widget.h"
#include "cards_filter.h"
#include "client_storage.h"
#include "qt_helpers.h"

MainWindow::MainWindow(const QString &deck_file) {
  setWindowTitle("Dumbcards");
  auto deck_menu = menuBar()->addMenu(tr("&Deck"));
  cards_menu = menuBar()->addMenu(tr("&Cards"));
  cards_menu->setEnabled(false);

  auto action_open_deck = new QAction(tr("Open"), this);
  action_open_deck->setShortcut(QKeySequence::Open);
  action_open_deck->setStatusTip(tr("Open Deck"));
  CONNECT(action_open_deck, triggered, this, openDeck);
  deck_menu->addAction(action_open_deck);

  auto action_filter = new QAction(tr("Filter"), this);
  action_filter->setShortcut(QKeySequence::Find);
  action_filter->setStatusTip(tr("Filter Cards"));
  CONNECT(action_filter, triggered, this, showFilter);
  cards_menu->addAction(action_filter);

  action_mark_correct = new QAction(tr("Mark the card as correct"), this);
  action_mark_correct->setShortcut(Qt::CTRL + Qt::Key_C);
  action_mark_correct->setStatusTip(
      tr("Mark current card as answered fully correct"));
  CONNECT(action_mark_correct, triggered, this, markCardCorrect);
  cards_menu->addAction(action_mark_correct);

  openFile(deck_file);
}

MainWindow::~MainWindow() = default;

void MainWindow::openDeck() {
  openFile(QFileDialog::getOpenFileName(this, tr("Open Deck File"), "",
                                        tr("Dumbcards Deck (*.dc)")));
}

void MainWindow::readCards() {
  cards.clear();

  while (true) {
    auto card = storage->getCard();
    if (!card) {
      break;
    }
    cards.push_back(std::move(card));
  }
  card_widget->show();
  card_widget->setCardsNum(cards.size());
  action_mark_correct->setEnabled(true);
  showCard();
}

void MainWindow::showCard() {
  if (cards.empty()) {
    card_widget->hide();
    action_mark_correct->setEnabled(false);
    return;
  }
  current_card = std::move(cards.front());
  cards.pop_front();
  card_widget->showCard(*current_card, cards.size());
}

void MainWindow::openFile(const QString &deck_file) {
  if (deck_file.isEmpty()) {
    return;
  }
  storage.reset();
  storage = std::make_unique<ClientStorage>(deck_file);
  if (!card_widget) {
    card_widget = new CardWidget();
    setCentralWidget(card_widget);
    CONNECT(card_widget, answered, this, answered);
  }
  card_widget->setDeckInfo(storage->getDeckInfo());
  readCards();
  cards_menu->setEnabled(true);
  updateTitle(storage->getDeckInfo().name);
}

void MainWindow::updateTitle(const QString &deck_name) {
  setWindowTitle(QString("%1 - Dumbcards").arg(deck_name));
}

void MainWindow::showFilter() {
  CardsFilter filter(this, storage->getTags(), storage->getFilter());
  if (filter.exec() != QDialog::Accepted) {
    return;
  }
  storage->setFilter(filter.getFilter());
  readCards();
}

void MainWindow::answered(bool correct) {
  storage->updateCard(*current_card, correct);
  if (!correct) {
    cards.push_back(std::move(current_card));
  }
  showCard();
}

void MainWindow::markCardCorrect() {
  auto ret =
      QMessageBox(QMessageBox::Question, tr("Mark Card As Correct"),
                  tr("Are you sure you want to mark this card as correct?"),
                  QMessageBox::Yes | QMessageBox::No, this)
          .exec();
  if (ret != QMessageBox::Yes) {
    return;
  }
  storage->markCardCorrect(*current_card);
  showCard();
}
