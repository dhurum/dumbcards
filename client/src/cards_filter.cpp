/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "cards_filter.h"
#include <QCheckBox>
#include <QComboBox>
#include <QDialogButtonBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>
#include "card_tags_selector.h"
#include "qt_helpers.h"

CardsFilter::CardsFilter(QWidget *parent, const std::vector<Tag> &tags,
                         const Filter &filter)
    : QDialog(parent) {
  auto layout = new QVBoxLayout();
  setLayout(layout);
  setWindowTitle(tr("Cards Filter"));

  layout->addWidget(new QLabel(tr("Show cards:")));

  correct = new QCheckBox(tr("Correctly answered"));
  layout->addWidget(correct);
  correct->setCheckState(filter.correct ? Qt::Checked : Qt::Unchecked);

  random = new QCheckBox(tr("In random order"));
  layout->addWidget(random);
  random->setCheckState(filter.random ? Qt::Checked : Qt::Unchecked);

  auto time_layout = new QHBoxLayout();
  time_layout->addWidget(new QLabel(tr("Answered earlier than:")));

  time = new QLineEdit();
  time_layout->addWidget(time);
  layout->addLayout(time_layout);
  time->setText(QString().setNum(filter.time_ago));

  time_units = new QComboBox();
  time_units->addItem(tr("minutes"));
  time_units->addItem(tr("hours"));
  time_units->addItem(tr("days"));
  time_units->setCurrentIndex(filter.time_units);
  time_layout->addWidget(time_units);

  auto limit_layout = new QHBoxLayout();
  limit_layout->addWidget(new QLabel(tr("Cards number limit:")));
  limit = new QLineEdit();
  limit_layout->addWidget(limit);
  layout->addLayout(limit_layout);
  limit->setText(QString().setNum(filter.limit));

  layout->addWidget(new QLabel(tr("With tags:")));
  tags_selector = new CardTagsSelector(tags, &filter.tags);
  layout->addWidget(tags_selector);

  tags_type = new QComboBox();
  tags_type->addItem(tr("With any of the tags"));
  tags_type->addItem(tr("With all of the tags"));
  layout->addWidget(tags_type);
  tags_type->setCurrentIndex(filter.all_tags ? 1 : 0);

  layout->addStretch();
  layout->addSpacing(20);

  auto button_box =
      new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
  layout->addWidget(button_box);

  auto ok_button = button_box->button(QDialogButtonBox::Ok);
  ok_button->setDefault(true);
  CONNECT(ok_button, clicked, this, check);
  auto cancel_button = button_box->button(QDialogButtonBox::Cancel);
  CONNECT(cancel_button, clicked, this, reject);
}

Filter CardsFilter::getFilter() {
  Filter filter;

  filter.correct = (correct->checkState() == Qt::Checked);
  filter.random = (random->checkState() == Qt::Checked);
  filter.time_ago = time->text().toInt();
  filter.time_units =
      static_cast<Filter::TimeUnits>(time_units->currentIndex());
  filter.limit = limit->text().toInt();
  filter.tags = tags_selector->get();
  filter.all_tags = (tags_type->currentIndex() == 1);

  return filter;
}

void CardsFilter::check() {
  bool number_ok = true;
  time->text().toInt(&number_ok);
  if (!time->text().isEmpty() && !number_ok) {
    QMessageBox(QMessageBox::Warning, tr("Invalid Data"),
                tr("Please, set correct number"), QMessageBox::Ok, this)
        .exec();
    time->setFocus();
    return;
  }
  if (time->text().isEmpty()) {
    time->setText("0");
  }

  limit->text().toInt(&number_ok);
  if (!limit->text().isEmpty() && !number_ok) {
    QMessageBox(QMessageBox::Warning, tr("Invalid Data"),
                tr("Please, set correct number"), QMessageBox::Ok, this)
        .exec();
    limit->setFocus();
    return;
  }
  if (limit->text().isEmpty()) {
    limit->setText("0");
  }
  accept();
}
