/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "word_card_widget.h"
#include <QVBoxLayout>
#include "word_card.h"
#include "wqlabel.h"

WordCardWidget::WordCardWidget() {
  layout = new QVBoxLayout();
  setLayout(layout);
}

void WordCardWidget::showCard(const Card &_card) {
  const auto &card = dynamic_cast<const WordCard &>(_card);

  if (key_field) {
    layout->removeWidget(key_field);
    delete key_field;
    key_field = nullptr;
  }
  for (auto field : fields) {
    layout->removeWidget(field);
    delete field;
  }
  fields.clear();

  for (size_t i = 0; i < std::min(word_fields->size(), card.fieldsNum()); ++i) {
    auto field_text = card.getField(i);
    if (field_text.isEmpty()) {
      continue;
    }
    auto field = new WQLabel(QString("%1: <font size=5>%2</font>")
                                 .arg((*word_fields)[i].name)
                                 .arg(field_text));
    if (i == card.keyFieldId()) {
      key_field = field;
      layout->addWidget(field);
    } else {
      fields.push_back(field);
    }
  }
}

void WordCardWidget::showFull() {
  for (auto field : fields) {
    layout->addWidget(field);
  }
}

void WordCardWidget::setDeckInfo(const DeckInfo &deck_info) {
  word_fields = &deck_info.word_fields;
}
