/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "table_card_widget.h"
#include <QHeaderView>
#include <QTableWidget>
#include <QVBoxLayout>
#include "table_card.h"
#include "wqlabel.h"

TableCardWidget::TableCardWidget() {
  layout = new QVBoxLayout();
  setLayout(layout);

  title = new WQLabel();
  layout->addWidget(title);

  appendix = new WQLabel();
  layout->addWidget(appendix);
  appendix->hide();
}

void TableCardWidget::showCard(const Card &_card) {
  const auto &card = dynamic_cast<const TableCard &>(_card);

  if (table) {
    layout->removeWidget(table);
    delete table;
  }
  title->setText(QString("<font size=5>%1</font>").arg(card.getName()));

  table = new QTableWidget(card.rowsNum(), card.colsNum());
  table->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
  table->verticalHeader()->hide();
  table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
  table->horizontalHeader()->hide();
  table->setSelectionMode(QAbstractItemView::NoSelection);
  table->setEditTriggers(QAbstractItemView::NoEditTriggers);
  table->setFocusPolicy(Qt::NoFocus);

  for (size_t i = 0; i < card.rowsNum(); ++i) {
    for (size_t j = 0; j < card.colsNum(); ++j) {
      auto cell = new QTableWidgetItem(card.getCell(i, j));
      table->setItem(i, j, cell);
    }
  }
  layout->insertWidget(1, table);
  table->hide();

  appendix->setText(card.getAppendix());
  appendix->hide();
}

void TableCardWidget::showFull() {
  table->show();
  appendix->show();
}
