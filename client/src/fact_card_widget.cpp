/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#include "fact_card_widget.h"
#include <QHeaderView>
#include <QVBoxLayout>
#include "fact_card.h"
#include "wqlabel.h"

FactCardWidget::FactCardWidget() {
  auto layout = new QVBoxLayout();
  setLayout(layout);

  title = new WQLabel();
  layout->addWidget(title);

  description = new WQLabel();
  layout->addWidget(description);
  description->hide();
}

void FactCardWidget::showCard(const Card &_card) {
  const auto &card = dynamic_cast<const FactCard &>(_card);

  title->setText(QString("<font size=5>%1</font>").arg(card.getName()));

  description->setText(card.getDescription());
  description->hide();
}

void FactCardWidget::showFull() {
  description->show();
}
