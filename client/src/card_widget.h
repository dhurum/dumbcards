/*******************************************************************************

Copyright 2016 Denis Tikhomirov

This file is part of Dumbcards

Dumbcards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dumbcards is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Dumbcards.  If not, see http://www.gnu.org/licenses/.

*******************************************************************************/

#pragma once

#include <QWidget>

class Card;
struct DeckInfo;
class QPushButton;
class QProgressBar;
class WordCardWidget;
class TableCardWidget;
class FactCardWidget;
class CommonCardWidget;

class CardWidget : public QWidget {
  Q_OBJECT

 public:
  CardWidget();
  void showCard(const Card &card, size_t remaining_cards);
  void setDeckInfo(const DeckInfo &deck_info);
  void setCardsNum(size_t cards_num);

 private:
  QPushButton *show_button;
  QPushButton *correct_button;
  QWidget *result_buttons;
  WordCardWidget *word_widget;
  TableCardWidget *table_widget;
  FactCardWidget *fact_widget;
  CommonCardWidget *current_card_widget;
  QProgressBar *progress_bar;
  size_t cards_num;

 private slots:
  void showFull();

 signals:
  void answered(bool correct);
};
